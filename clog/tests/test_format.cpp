#include <clog_format>

#include <iostream>
#include <map>

#include "catch.hpp"

enum class TestEnum
{
  Enum1,
  Enum2
};

struct convertible_to_string
{
  operator std::string() const { return std::string("me"); }
};

template<>
struct std::formatter<convertible_to_string>
    : public clog_format::cast_formatter<convertible_to_string, std::string>
{
};

clog_format_declare_enum_formatter(TestEnum, Enum1, Enum2);

struct not_formattable
{
};

static_assert(clog_format::is_formattable_v<convertible_to_string>);
static_assert(clog_format::is_formattable_v<std::vector<convertible_to_string>>);
static_assert(not clog_format::is_formattable_v<not_formattable>);
static_assert(not clog_format::is_formattable_v<std::vector<not_formattable>>);
static_assert(clog_format::is_formattable_v<TestEnum>);

struct hello
{
};

clog_format_declare_formatter(const hello*)
{
  (void)p;
  return std::format_to(ctx.out(), "hello*");
}

TEST_CASE("test_format_enum", "[format,enum]")
{
  CHECK(std::format("It is {}!", TestEnum::Enum1) == "It is Enum1!");
}

TEST_CASE("test_format_vector", "[format,vector]")
{
  CHECK(std::format("{}", std::vector<int>{1, 2, 3}) == "[1, 2, 3]");
  CHECK(std::format("{:c}", std::vector<int>{1, 2, 3}) == "{1, 2, 3}");
  CHECK(std::format("{:s' : '}", std::vector<int>{1, 2, 3}) == "[1 : 2 : 3]");
}

TEST_CASE("test_format_map", "[format,vector]")
{
  std::map<std::string, int> obj{{"a", 1}, {"b", 2}};
  CHECK(std::format("{}", obj) == "{a: 1, b: 2}");
  CHECK(std::format("{:c}", obj) == "[a: 1, b: 2]");
}

TEST_CASE("test_format_cast", "[format,cast]")
{
  CHECK(std::format("It is {}!", convertible_to_string()) == "It is me!");
}

TEST_CASE("test_format_pointer", "[format,pointer]")
{
  hello h;
  const hello* hp = &h;
  CHECK(std::format("It is {}!", hp) == "It is hello*!");
}
