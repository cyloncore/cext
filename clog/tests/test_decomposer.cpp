#include <clog>

#include "catch.hpp"

#define MAKE_DECOMPOSER(...) (clog_impl::decomposer() << __VA_ARGS__ << clog_impl::decomposer())

#define CHECK_DECOMPOSER(_RES_, _STRING_, ...)                                                     \
  {                                                                                                \
    cext_clang_pragma(diagnostic push);                                                            \
    cext_clang_pragma(diagnostic ignored "-Woverloaded-shift-op-parentheses");                     \
    CHECK(clog_format::to_string(MAKE_DECOMPOSER(__VA_ARGS__)) == _STRING_);                       \
    CHECK((MAKE_DECOMPOSER(__VA_ARGS__).result() == _RES_));                                       \
    cext_clang_pragma(clang diagnostic pop);                                                       \
  }

struct TestStruct
{
  TestStruct* right = nullptr;
};

void function() {}

static_assert(clog_impl::is_pointer_v<void*>);
static_assert(not clog_impl::is_pointer_v<int>);
static_assert(clog_impl::is_pointer_v<clog_impl::unary_expr<void*>>);
static_assert(clog_impl::is_pointer_v<clog_impl::unary_expr<void*&>>);
static_assert(clog_impl::is_pointer_v<clog_impl::unary_expr<const void*&>>);
static_assert(not clog_impl::is_pointer_v<clog_impl::unary_expr<bool>>);

enum Status
{
  Invalid,
  Finished,
  Ready
};

clog_format_declare_enum_formatter(Status, Invalid, Finished, Ready);

int __attribute__((noinline)) result() { return 2; }

Status __attribute__((noinline)) status() { return Status::Ready; }

TEST_CASE("decomposer", "[decomposer]")
{
  CHECK_DECOMPOSER(true, "1", 1);
  CHECK_DECOMPOSER(true, "1 < 2", 1 < 2);
  CHECK_DECOMPOSER(false, "0x0", (TestStruct*)nullptr);
  CHECK_DECOMPOSER(false, "true && false", true && false);
  bool a = true;
  bool b = false;
  CHECK_DECOMPOSER(false, "true && false", a && b);
  CHECK_DECOMPOSER(false, "true && 1 > 2", a && 1 > 2);
  CHECK_DECOMPOSER(false, "1 > 2 && 1 < 2", 1 > 2 && 1 < 2);
  CHECK_DECOMPOSER(false, "1 > 2 && true", 1 > 2 && a);
  CHECK_DECOMPOSER(true, "1 > 2 || true", 1 > 2 || a);
  CHECK_DECOMPOSER(false, "1 > 2 || true && false", 1 > 2 || a && false);
  //   CHECK_DECOMPOSER(false, "1 > 2 || true && false == true", 1 > 2 || a == true && false ==
  //   true);

  CHECK_DECOMPOSER(true, "1 == 1", (long unsigned int)1 == (unsigned int)1);
  CHECK_DECOMPOSER(false, "1 != 1", (long unsigned int)1 != (unsigned int)1);

  int type = 2;
  CHECK_DECOMPOSER(false, "2 < 1", type < 1);
  CHECK_DECOMPOSER(false, "2 < 1 || 2 < 2", type < 1 or type < 2);
  CHECK_DECOMPOSER(false, "2 == 1", type == 1);
  CHECK_DECOMPOSER(true, "2 == 1 || 2 == 2", type == 1 or type == 2);
  CHECK_DECOMPOSER(true, "2 == 1 || true || false || false || false || false || 2 == 7",
                   type == 1 or type == 2 or type == 3 or type == 4 or type == 5 or type == 6
                     or type == 7);
  CHECK_DECOMPOSER(false, "2 != 1 && 2 != 2", type != 1 and type != 2);
  CHECK_DECOMPOSER(false, "2 != 1 && 2 != 2", result() != 1 and result() != 2);
  CHECK_DECOMPOSER(false, "Ready != Finished && Ready != Ready",
                   status() != Status::Finished and status() != Status::Ready);

  auto f_ptr = &function;
  union
  {
    decltype(f_ptr) f;
    void* p;
  } c;
  c.f = f_ptr;
  CHECK_DECOMPOSER(true, clog_format::to_string(c.p), f_ptr);

  TestStruct* ts_left = new TestStruct;
  TestStruct* ts_right = nullptr;

  CHECK_DECOMPOSER(true, "false || 0x0 == 0x0", not ts_left or ts_left->right == ts_right);

  auto expr = clog_eval_expr(1 < 2);

  CHECK(clog_format::to_string(expr) == "1 < 2 (true = 1 < 2)");
  CHECK(expr.value() == true);

  //   MAKE_DECOMPOSER(ts and ts->a);
  //   MAKE_DECOMPOSER(ts and ts->a < 0);

  //   CHECK_DECOMPOSER(false, "", ts and ts->a);
}
