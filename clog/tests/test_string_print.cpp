#include "catch.hpp"
#include <clog>

TEST_CASE("string_format", "[string_format,to_string]")
{
  CHECK(std::format("hello") == "hello");
  CHECK(std::format("{}", "hello") == "hello");
  CHECK(std::format("{} world", "hello") == "hello world");
  CHECK(std::format("{}", "hello") == "hello");
  CHECK(std::format("{}{{}}", "hello") == "hello{}");
  CHECK(std::format("{} world", "hello") == "hello world");
  CHECK(std::format("{}{{}} world", "hello") == "hello{} world");
  CHECK(std::format("{} {}", "hello", "world") == "hello world");
}
