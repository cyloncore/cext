#include <clog>

#include "catch.hpp"

#include <cstring>

#include <sqlite3.h>

struct tmp_file
{
  ~tmp_file() { std::remove(name1.c_str()); }
  std::string name1 = std::tmpnam(nullptr);
};

void test_s_r(sqlite3_stmt* _ps, int _level, int64_t _start_time, int64_t _end_time,
              const std::string& _filename, int _linenumber, const std::string& _message)
{
  CHECK(sqlite3_step(_ps) == SQLITE_ROW);
  CHECK(sqlite3_column_int64(_ps, 0) == _level);
  CHECK(sqlite3_column_int64(_ps, 1) > _start_time);
  CHECK(sqlite3_column_int64(_ps, 1) < _end_time);
  CHECK(std::string(reinterpret_cast<const char*>(sqlite3_column_text(_ps, 2))) == _filename);
  CHECK(sqlite3_column_int64(_ps, 3) == _linenumber);
  CHECK(std::string(reinterpret_cast<const char*>(sqlite3_column_text(_ps, 4))) == _message);
}

TEST_CASE("sqlite", "[sqlite]")
{
  tmp_file name1;
  int64_t start_time
    = std::chrono::time_point_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now())
        .time_since_epoch()
        .count();
  clog_impl::sqlite_logging_listener sll(name1.name1);
  sll.report_debug("a", 123, "b");
  sll.report_error("c", 456, "d");
  sll.report_info("e", 789, "f");
  sll.report_fatal("g", 012, "h");
  sll.report_warning("i", 345, "j");
  int64_t end_time
    = std::chrono::time_point_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now())
        .time_since_epoch()
        .count();

  sqlite3* handle = nullptr;
  CHECK(sqlite3_open(name1.name1.c_str(), &handle) == SQLITE_OK);

  const char* query = R"""(SELECT level, timesamp, filename, line, message FROM clog_logs)""";
  sqlite3_stmt* ps;
  CHECK(sqlite3_prepare_v2(handle, query, strlen(query), &ps, nullptr) == SQLITE_OK);

  test_s_r(ps, 0, start_time, end_time, "a", 123, "b");
  test_s_r(ps, 1, start_time, end_time, "c", 456, "d");
  test_s_r(ps, 2, start_time, end_time, "e", 789, "f");
  test_s_r(ps, 3, start_time, end_time, "g", 012, "h");
  test_s_r(ps, 4, start_time, end_time, "i", 345, "j");

  sqlite3_finalize(ps);
  sqlite3_close(handle);
}
