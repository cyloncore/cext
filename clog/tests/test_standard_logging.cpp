#include <clog>

#include <iostream>
#include <sstream>

#include "catch.hpp"
#include "common.h"

TEST_CASE("standard_logging", "[standard_logging]")
{
  CHECK(ends_of("hello world", 5) == "world");
  CHECK_END_WITH("hello world", "world");

  std::stringbuf sb;
  {
    cerr_redirect cr(&sb);
    clog_error("hello");
  }
  CHECK_END_WITH(sb.str(), "tests/test_standard_logging.cpp:" + std::to_string(__LINE__ - 2)
                             + ": error:hello\n");
  {
    cerr_redirect cr(&sb);
    clog_error("hello {}", "world");
  }
  CHECK_END_WITH(sb.str(), "tests/test_standard_logging.cpp:" + std::to_string(__LINE__ - 2)
                             + ": error:hello world\n");
  {
    cerr_redirect cr(&sb);
    clog_error("hello {}", std::string("world"));
  }
  CHECK_END_WITH(sb.str(), "tests/test_standard_logging.cpp:" + std::to_string(__LINE__ - 2)
                             + ": error:hello world\n");
  // Test debug
  {
    cerr_redirect cr(&sb);
    clog_debug("hello");
  }
  CHECK_END_WITH(sb.str(), "tests/test_standard_logging.cpp:" + std::to_string(__LINE__ - 2)
                             + ": debug:hello\n");
  {
    cerr_redirect cr(&sb);
    clog_debug("hello {}", "world");
  }
  CHECK_END_WITH(sb.str(), "tests/test_standard_logging.cpp:" + std::to_string(__LINE__ - 2)
                             + ": debug:hello world\n");
  {
    cerr_redirect cr(&sb);
    clog_debug("hello {}", std::string("world"));
  }
  CHECK_END_WITH(sb.str(), "tests/test_standard_logging.cpp:" + std::to_string(__LINE__ - 2)
                             + ": debug:hello world\n");
}
