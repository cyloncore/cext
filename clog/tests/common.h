
namespace
{
  struct cerr_redirect
  {
    cerr_redirect(std::streambuf* new_buffer) : old(std::cerr.rdbuf(new_buffer)) {}

    ~cerr_redirect() { std::cerr.rdbuf(old); }
  private:
    std::streambuf* old;
  };
  struct cout_redirect
  {
    cout_redirect(std::streambuf* new_buffer) : old(std::cout.rdbuf(new_buffer)) {}

    ~cout_redirect() { std::cout.rdbuf(old); }
  private:
    std::streambuf* old;
  };
} // namespace

inline bool ends_with(const std::string& fullString, const std::string& ending)
{
  if(fullString.length() >= ending.length())
  {
    return (0
            == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
  }
  else
  {
    return false;
  }
}

inline std::string ends_of(const std::string& _str, std::size_t _l)
{
  return _str.substr(_str.size() - _l);
}

#define CHECK_END_WITH(_STR_1_, _STR_2_)                                                           \
  {                                                                                                \
    std::size_t _ = std::string(_STR_2_).size();                                                   \
    CHECK(ends_of(_STR_1_, _) == _STR_2_);                                                         \
  }
