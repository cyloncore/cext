#include <clog_qt>

#include "catch.hpp"

TEST_CASE("test_format_qt", "[format,qt]")
{
  CHECK(std::format("It is {}!", QString("just me")) == "It is just me!");
  CHECK(std::format("It is {}!", QStringList({"just me", "and you"}))
        == "It is [just me, and you]!");
  CHECK(std::format("It is {}!", QByteArray("just me")) == "It is just me!");
}
