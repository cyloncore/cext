#pragma once

// The following macros are for internal use by clog, no warranty of behavior is guaranteed

#define __clog_inc(I) __clog_inc_##I
#define __clog_inc_0 1
#define __clog_inc_1 2
#define __clog_inc_2 3
#define __clog_inc_3 4
#define __clog_inc_4 5
#define __clog_inc_5 6
#define __clog_inc_6 7
#define __clog_inc_7 8
#define __clog_inc_8 9
#define __clog_inc_9 10
#define __clog_inc_10 11
#define __clog_inc_11 12
#define __clog_inc_12 13
#define __clog_inc_13 14
#define __clog_inc_14 15
#define __clog_inc_15 16
#define __clog_inc_16 17
#define __clog_inc_17 18
#define __clog_inc_18 19
#define __clog_inc_19 20
#define __clog_inc_20 21
#define __clog_inc_21 22
#define __clog_inc_22 23
#define __clog_inc_23 24
#define __clog_inc_24 25
#define __clog_inc_25 26
#define __clog_inc_26 27
#define __clog_inc_27 28
#define __clog_inc_28 29
#define __clog_inc_29 30
#define __clog_inc_30 31
#define __clog_inc_31 32
#define __clog_inc_32 33
#define __clog_inc_33 34
#define __clog_inc_34 35
#define __clog_inc_35 36
#define __clog_inc_36 37
#define __clog_inc_37 38
#define __clog_inc_38 39
#define __clog_inc_39 40
#define __clog_inc_40 41
#define __clog_inc_41 42
#define __clog_inc_42 43
#define __clog_inc_43 44
#define __clog_inc_44 45
#define __clog_inc_45 46
#define __clog_inc_46 47
#define __clog_inc_47 48
#define __clog_inc_48 59
#define __clog_inc_49 50
#define __clog_inc_50 51
#define __clog_inc_51 52
#define __clog_inc_52 53
#define __clog_inc_53 54
#define __clog_inc_54 55
#define __clog_inc_55 56
#define __clog_inc_56 57
#define __clog_inc_57 58
#define __clog_inc_58 59
#define __clog_inc_59 60
#define __clog_inc_60 61
#define __clog_inc_61 62
#define __clog_inc_62 63
#define __clog_inc_63 64
#define __clog_inc_64 65
#define __clog_inc_65 66
#define __clog_inc_66 67
#define __clog_inc_67 68
#define __clog_inc_68 69
#define __clog_inc_69 70
#define __clog_inc_70 71
#define __clog_inc_71 72
#define __clog_inc_72 73
#define __clog_inc_73 74
#define __clog_inc_74 75
#define __clog_inc_75 76
#define __clog_inc_76 77
#define __clog_inc_77 78
#define __clog_inc_78 79
#define __clog_inc_79 80
#define __clog_inc_80 81
#define __clog_inc_81 82
#define __clog_inc_82 83
#define __clog_inc_83 84
#define __clog_inc_84 85
#define __clog_inc_85 86
#define __clog_inc_86 87
#define __clog_inc_87 88
#define __clog_inc_88 89
#define __clog_inc_89 90
#define __clog_inc_90 91

#define __clog_foreach__1(WHAT, I, X) WHAT(X, I)
#define __clog_foreach__2(WHAT, I, X, ...)                                                         \
  WHAT(X, I) __clog_foreach__1(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__3(WHAT, I, X, ...)                                                         \
  WHAT(X, I) __clog_foreach__2(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__4(WHAT, I, X, ...)                                                         \
  WHAT(X, I) __clog_foreach__3(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__5(WHAT, I, X, ...)                                                         \
  WHAT(X, I) __clog_foreach__4(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__6(WHAT, I, X, ...)                                                         \
  WHAT(X, I) __clog_foreach__5(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__7(WHAT, I, X, ...)                                                         \
  WHAT(X, I) __clog_foreach__6(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__8(WHAT, I, X, ...)                                                         \
  WHAT(X, I) __clog_foreach__7(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__9(WHAT, I, X, ...)                                                         \
  WHAT(X, I) __clog_foreach__8(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__10(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__9(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__11(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__10(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__12(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__11(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__13(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__12(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__14(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__13(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__15(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__14(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__16(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__15(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__17(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__16(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__18(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__17(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__19(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__18(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__20(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__19(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__21(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__20(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__22(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__21(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__23(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__22(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__24(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__23(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__25(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__24(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__26(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__25(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__27(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__26(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__28(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__27(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__29(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__28(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__30(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__29(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__31(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__30(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__32(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__31(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__33(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__32(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__34(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__33(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__35(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__34(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__36(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__35(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__37(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__36(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__38(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__37(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__39(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__38(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__40(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__39(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__41(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__40(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__42(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__41(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__43(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__42(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__44(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__43(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__45(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__44(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__46(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__45(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__47(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__46(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__48(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__47(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__49(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__48(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__50(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__49(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__51(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__50(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__52(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__51(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__53(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__52(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__54(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__53(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__55(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__54(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__56(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__55(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__57(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__56(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__58(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__57(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__59(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__58(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__60(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__59(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__61(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__60(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__62(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__61(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__63(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__62(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__64(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__63(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__65(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__64(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__66(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__65(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__67(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__66(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__68(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__67(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__69(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__68(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__70(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__69(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__71(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__70(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__72(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__71(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__73(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__72(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__74(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__73(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__75(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__74(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__76(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__75(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__77(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__76(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__78(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__77(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__79(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__78(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__80(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__79(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__81(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__80(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__82(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__81(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__83(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__82(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__84(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__83(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__85(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__84(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__86(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__85(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__87(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__86(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__88(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__87(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__89(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__88(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__90(WHAT, I, X, ...)                                                        \
  WHAT(X, I) __clog_foreach__89(WHAT, __clog_inc(I), __VA_ARGS__)

#define __clog_get_macro(                                                                          \
  _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21,  \
  _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40,   \
  _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59,   \
  _60, _61, _62, _63, _64, _65, _66, _67, _68, _69, _70, _71, _72, _73, _74, _75, _76, _77, _78,   \
  _79, _80, _81, _82, _83, _84, _85, _86, _87, _88, _89, NAME, ...)                                \
  NAME

#define __clog_foreach(action, ...)                                                                \
  __clog_get_macro(__VA_ARGS__, __clog_foreach__89, __clog_foreach__88, __clog_foreach__87,        \
                   __clog_foreach__86, __clog_foreach__85, __clog_foreach__84, __clog_foreach__83, \
                   __clog_foreach__82, __clog_foreach__81, __clog_foreach__80, __clog_foreach__79, \
                   __clog_foreach__78, __clog_foreach__77, __clog_foreach__76, __clog_foreach__75, \
                   __clog_foreach__74, __clog_foreach__73, __clog_foreach__72, __clog_foreach__71, \
                   __clog_foreach__70, __clog_foreach__69, __clog_foreach__68, __clog_foreach__67, \
                   __clog_foreach__66, __clog_foreach__65, __clog_foreach__64, __clog_foreach__63, \
                   __clog_foreach__62, __clog_foreach__61, __clog_foreach__60, __clog_foreach__59, \
                   __clog_foreach__58, __clog_foreach__57, __clog_foreach__56, __clog_foreach__55, \
                   __clog_foreach__54, __clog_foreach__53, __clog_foreach__52, __clog_foreach__51, \
                   __clog_foreach__50, __clog_foreach__49, __clog_foreach__48, __clog_foreach__47, \
                   __clog_foreach__46, __clog_foreach__45, __clog_foreach__44, __clog_foreach__43, \
                   __clog_foreach__42, __clog_foreach__41, __clog_foreach__40, __clog_foreach__39, \
                   __clog_foreach__38, __clog_foreach__37, __clog_foreach__36, __clog_foreach__35, \
                   __clog_foreach__34, __clog_foreach__33, __clog_foreach__32, __clog_foreach__31, \
                   __clog_foreach__30, __clog_foreach__29, __clog_foreach__28, __clog_foreach__27, \
                   __clog_foreach__26, __clog_foreach__25, __clog_foreach__24, __clog_foreach__23, \
                   __clog_foreach__22, __clog_foreach__21, __clog_foreach__20, __clog_foreach__19, \
                   __clog_foreach__18, __clog_foreach__17, __clog_foreach__16, __clog_foreach__15, \
                   __clog_foreach__14, __clog_foreach__13, __clog_foreach__12, __clog_foreach__11, \
                   __clog_foreach__10, __clog_foreach__9, __clog_foreach__8, __clog_foreach__7,    \
                   __clog_foreach__6, __clog_foreach__5, __clog_foreach__4, __clog_foreach__3,     \
                   __clog_foreach__2, __clog_foreach__1)(action, 0, __VA_ARGS__)

#define __clog_select(_WHAT_, ...) _WHAT_
