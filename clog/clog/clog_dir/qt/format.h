#include <format>

namespace clog_qt
{
  template<typename... Args>
  inline std::string format(QString const& format_str, Args&&... args)
  {
    return ::std::vformat(format_str.toStdString(), std::make_format_args(args...));
  }
  template<typename... Args>
  inline std::string vformat(QString const& format_str, Args&&... args)
  {
    return ::std::vformat(format_str.toStdString(), args...);
  }
  inline QString qformat(QString const& format_str) { return format_str; }
  template<typename... Args>
  inline QString qformat(QString const& format_str, Args&&... args)
  {
    return QString::fromStdString(format(format_str, args...));
  }
  template<typename... Args>
  inline QString qvformat(QString const& format_str, Args&&... args)
  {
    return QString::fromStdString(vformat(format_str, args...));
  }
  template<typename T>
  inline QString to_qstring(T const& value)
  {
    return qformat("{}", value);
  }
} // namespace clog_qt

#include <QDebug>
#include <clog_format>

template<typename _T_>
  requires(clog_format::is_formattable_v<_T_> and not std::is_same_v<_T_, QVariant>)
QDebug operator<<(QDebug _debug, _T_ const& _object)
{
  _debug << clog_qt::to_qstring(_object);
  return _debug;
}
