#include <format>

#include <QDebug>

#include <QCborValue>
#include <QDateTime>
#include <QJsonValue>
#include <QList>
#include <QPointF>
#include <QSharedPointer>
#include <QUuid>
#include <QVariant>

template<typename T>
struct std::formatter<QAtomicInteger<T>> : public std::formatter<T>
{
  using super = std::formatter<T>;
  template<typename FormatContext>
  FormatContext::iterator format(QAtomicInteger<T> const& p, FormatContext& ctx) const
  {
    return super::format(p, ctx);
  }
};

template<>
struct std::formatter<QAtomicInt> : public std::formatter<int>
{
  using super = std::formatter<int>;
  template<typename FormatContext>
  FormatContext::iterator format(QAtomicInt const& p, FormatContext& ctx) const
  {
    return super::format(p, ctx);
  }
};

template<>
struct std::formatter<QPoint> : public std::formatter<int>
{
  template<typename FormatContext>
  FormatContext::iterator format(const QPoint& p, FormatContext& ctx) const
  {
    return format_to(ctx.out(), "({}, {})", std::formatter<int>::format(p.x(), ctx),
                     std::formatter<int>::format(p.y(), ctx));
  }
};

template<>
struct std::formatter<QPointF> : public std::formatter<qreal>
{
  template<typename FormatContext>
  FormatContext::iterator format(const QPointF& p, FormatContext& ctx) const
  {
    return format_to(ctx.out(), "({}, {})", std::formatter<qreal>::format(p.x(), ctx),
                     std::formatter<qreal>::format(p.y(), ctx));
  }
};

namespace clog_qt::details
{
  namespace ci = clog_impl;
  template<typename T>
  inline constexpr bool is_qt_smart_pointer_v
    = ci::is_instantiation_of_v<QExplicitlySharedDataPointer, T>
      or ci::is_instantiation_of_v<QSharedPointer, T>;
} // namespace clog_qt::details

template<typename _T_, typename Char>
  requires clog_qt::details::is_qt_smart_pointer_v<_T_>
struct std::formatter<_T_, Char> : clog_format::base_formatter
{
  template<typename FormatContext>
  FormatContext::iterator format(const _T_& p, FormatContext& ctx) const
  {
    return forward(p.data(), ctx);
  }
};

template<typename _T_>
  requires clog_qt::details::is_qt_smart_pointer_v<_T_>
struct clog_impl::map_for_expr_printing<_T_>
{
  static const void* map(const _T_& _t) { return _t.data(); }
};

template<>
struct std::formatter<QString> : std::formatter<std::string>
{
  template<typename FormatContext>
  FormatContext::iterator format(const QString& p, FormatContext& ctx) const
  {
    return std::formatter<std::string>::format(p.toStdString(), ctx);
  }
};

template<>
struct std::formatter<QByteArray> : std::formatter<std::string>
{
  template<typename FormatContext>
  FormatContext::iterator format(const QByteArray& p, FormatContext& ctx) const
  {
    return std::formatter<std::string>::format(p.toStdString(), ctx);
  }
};

namespace clog_qt::details
{
  template<class>
  struct sfinae_true : std::true_type
  {
  };

  template<class T>
  static auto test_toString(int) -> sfinae_true<decltype(std::declval<T>().toString())>;
  template<class>
  static auto test_toString(long) -> std::false_type;

  template<class T>
  struct has_toString : decltype(test_toString<T>(0))
  {
  };
} // namespace clog_qt::details

static_assert(clog_qt::details::has_toString<QStringView>::value);
static_assert(clog_qt::details::has_toString<QUuid>::value);
static_assert(clog_qt::details::has_toString<QVariant>::value);

template<typename _T_, typename Char>
  requires clog_qt::details::has_toString<_T_>::value
struct std::formatter<_T_, Char> : std::formatter<QString>
{
  template<typename FormatContext>
  FormatContext::iterator format(const _T_& p, FormatContext& ctx) const
  {
    return std::formatter<QString>::format(p.toString(), ctx);
  }
};

template<>
struct std::formatter<QChar> : std::formatter<QString>
{
  template<typename FormatContext>
  FormatContext::iterator format(const QChar& p, FormatContext& ctx) const
  {
    return std::formatter<QString>::format(QString(p), ctx);
  }
};

template<>
struct std::formatter<QDateTime> : std::formatter<QString>
{
  template<typename FormatContext>
  FormatContext::iterator format(const QDateTime& p, FormatContext& ctx) const
  {
    return std::formatter<QString>::format(p.toString(Qt::ISODate), ctx);
  }
};

template<typename _T_>
  requires clog_format::is_formattable_v<_T_>
struct std::formatter<QList<_T_>> : public clog_format::base_iterable_formatter<QList<_T_>>
{
};

namespace clog_format
{

  namespace details
  {
    template<class T>
    concept is_qhash = requires(T x) {
      { QHash{x} } -> std::same_as<T>;
    };
    template<class T>
    concept is_qmap = requires(T x) {
      { QMap{x} } -> std::same_as<T>;
    };
  } // namespace details

  template<typename _T_>
    requires(details::is_qhash<_T_> or details::is_qmap<_T_>)
  class as_kv_range<_T_>
  {
  public:
    as_kv_range(const _T_& data) : m_data{data} {}
    auto begin() { return m_data.keyValueBegin(); }
    auto end() { return m_data.keyValueEnd(); }
  private:
    const _T_& m_data;
  };

} // namespace clog_format

template<typename _K_, typename _V_>
  requires clog_format::is_formattable_v<_K_> and clog_format::is_formattable_v<_V_>
struct std::formatter<QHash<_K_, _V_>> : public clog_format::base_map_formatter<QHash<_K_, _V_>>
{
};

template<typename _K_, typename _V_>
  requires clog_format::is_formattable_v<_K_> and clog_format::is_formattable_v<_V_>
struct std::formatter<QMap<_K_, _V_>> : public clog_format::base_map_formatter<QMap<_K_, _V_>>
{
};

namespace clog_qt::details
{
  template<typename _T_>
  struct qdebug_formatter : public std::formatter<QString>
  {
    template<typename FormatContext>
    FormatContext::iterator format(const _T_& p, FormatContext& ctx) const
    {
      QString errMsg;
      QDebug err(&errMsg);
      err.nospace() << p;
      return std::formatter<QString>::format(errMsg, ctx);
    }
  };
} // namespace clog_qt::details

#define CLOG_QT_DEBUG_FORMATTER(_TYPE_)                                                            \
  template<>                                                                                       \
  struct std::formatter<_TYPE_> : public clog_qt::details::qdebug_formatter<_TYPE_>                \
  {                                                                                                \
  };

CLOG_QT_DEBUG_FORMATTER(QCborValue)
CLOG_QT_DEBUG_FORMATTER(QJsonValue)
CLOG_QT_DEBUG_FORMATTER(QJsonObject)
CLOG_QT_DEBUG_FORMATTER(QJsonDocument)

#undef CLOG_QT_DEBUG_FORMATTER
