#include "loki_logging_listener.h"

#include <curl/curl.h>

#include <chrono>
#include <format>
#include <iostream>
#include <vector>

namespace
{
  std::string replace_all(const std::string& s, const std::string& toReplace,
                          std::string const& replaceWith)
  {
    std::string buf;
    std::size_t pos = 0;
    std::size_t prevPos;

    // Reserves rough estimate of final size of string.
    buf.reserve(s.size());

    while(true)
    {
      prevPos = pos;
      pos = s.find(toReplace, pos);
      if(pos == std::string::npos)
        break;
      buf.append(s, prevPos, pos - prevPos);
      buf += replaceWith;
      pos += toReplace.size();
    }

    buf.append(s, prevPos, s.size() - prevPos);
    return buf;
  }
} // namespace
namespace
{
  static int writer(char* data, size_t size, size_t nmemb, std::string* writerData)
  {
    if(writerData == NULL)
    {
      return 0;
    }

    writerData->append(data, size * nmemb);

    return size * nmemb;
  }
} // namespace
using namespace clog_impl;

struct loki_logging_listener::data
{
  CURL* curl = nullptr;
  std::unordered_map<std::string, std::string> extra_labels;
  std::string extra_labels_string, logger_name;
  std::string hostname, username, password;
  void report(const char* _severity, const std::string& _filename, int _line,
              const std::string& _message)
  {
    using time_stamp = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    time_stamp ts
      = std::chrono::time_point_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now());
    std::string payload = std::format(
      R"(
        {{
          "streams": [
            {{
              "stream": {{
                "severity": "{}",
                "logger": "{}",
                "filename": "{}",
                "line": {}
                {}
            }},
              "values": [["{}", "{}"]]
           }}
          ]
        }}
      )",
      _severity, logger_name, _filename, _line, extra_labels_string, ts.time_since_epoch().count(),
      replace_all(_message, "\"", "\\\""));
    std::cout << payload << std::endl;
    curl_easy_setopt(curl, CURLOPT_URL, (hostname + "/loki/api/v1/push").c_str());

    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, payload.size());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload.c_str());

    struct curl_slist* list = NULL;
    list = curl_slist_append(list, "Content-Type: application/json");
    list = curl_slist_append(list, "X-Scope-OrgID: com.cyloncore.clog");
    std::string buffer;
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);

    CURLcode res = curl_easy_perform(curl);
    if(res != CURLE_OK)
    {
      long response_code;
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
      std::cerr << "Failed to send log to " << hostname << ": " << curl_easy_strerror(res)
                << " error code: " << response_code << std::endl;
    }
    curl_slist_free_all(list);
  }
};

loki_logging_listener::loki_logging_listener(
  const std::string& _hostname, const std::string& _logger_name,
  const std::unordered_map<std::string, std::string>& _extra_labels, const std::string& _username,
  const std::string& _password)
    : d(new data)
{
  d->hostname = _hostname;
  d->username = _username;
  d->password = _password;
  d->logger_name = _logger_name;
  d->extra_labels = _extra_labels;

  for(const auto& [k, v] : _extra_labels)
  {
    d->extra_labels_string += std::format(", '{}': '{}' ", k, v);
  }

  curl_global_init(CURL_GLOBAL_ALL);
  d->curl = curl_easy_init();
}

loki_logging_listener::~loki_logging_listener()
{
  curl_easy_cleanup(d->curl);
  delete d;
}

void loki_logging_listener::report_debug(const std::string& _filename, int _line,
                                         const std::string& _message)
{
  d->report("debug", _filename, _line, _message);
}

void loki_logging_listener::report_error(const std::string& _filename, int _line,
                                         const std::string& _message)
{
  d->report("error", _filename, _line, _message);
}

void loki_logging_listener::report_info(const std::string& _filename, int _line,
                                        const std::string& _message)
{
  d->report("info", _filename, _line, _message);
}

void loki_logging_listener::report_fatal(const std::string& _filename, int _line,
                                         const std::string& _message)
{
  d->report("fatal", _filename, _line, _message);
}

void loki_logging_listener::report_warning(const std::string& _filename, int _line,
                                           const std::string& _message)
{
  d->report("warning", _filename, _line, _message);
}
