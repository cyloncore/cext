/*
 *  Inspired from Catch
 *  ----------------------------------------------------------
 *  Copyright (c) 2020 Two Blue Cubes Ltd. All rights reserved.
 *
 *  Distributed under the Boost Software License, Version 1.0. (See accompanying
 *  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#include <functional>

namespace clog_impl
{

  /**
   * Use to define mapping between expression value and display value in assertions
   */
  template<typename _T_, typename _Enabled_ = void>
  struct map_for_expr_printing
  {
    static const _T_& map(const _T_& _t) { return _t; }
  };

  /**
   * Handle pointer.
   */
  template<typename _T_>
  struct map_for_expr_printing<_T_*>
  {
    static const void* map(const _T_* _t) { return _t; }
  };

  /**
   * Handle function pointer.
   */
  template<typename _T_, typename... _TArgs_>
  struct map_for_expr_printing<_T_ (*)(_TArgs_...)>
  {
    static const void* map(_T_ (*_t)(_TArgs_...))
    {
      union
      {
        decltype(_t) f;
        void* p;
      } a;
      a.f = _t;
      return a.p;
    }
  };
  /**
   * Handle function pointer.
   */
  template<typename _T_, typename... _TArgs_>
  struct map_for_expr_printing<std::function<_T_(_TArgs_...)>>
  {
    static const void* map(const std::function<_T_(_TArgs_...)>& _t) { return _t ? &_t : nullptr; }
  };

  template<typename T>
  struct always_false : std::false_type
  {
  };

  template<class T>
  struct remove_cvref
  {
    typedef std::remove_cv_t<std::remove_reference_t<T>> type;
  };
  template<class T>
  using remove_cvref_t = typename remove_cvref<T>::type;

  template<template<typename...> class Template, typename T>
  struct is_instantiation_of : std::false_type
  {
  };

  template<template<typename...> class Template, typename... Args>
  struct is_instantiation_of<Template, Template<Args...>> : std::true_type
  {
  };

  template<template<typename...> class Template, typename... Args>
  inline constexpr bool is_instantiation_of_v = is_instantiation_of<Template, Args...>::value;

  template<typename T>
  class unary_expr;

  template<typename LhsT, typename RhsT, typename OpT>
  class binary_expr;

  template<typename T>
  inline constexpr bool is_instantiation_of_expr_v
    = is_instantiation_of_v<unary_expr, T> or is_instantiation_of_v<binary_expr, T>;

  template<typename T>
  inline constexpr bool is_not_arithmetic_and_not_expr_v
    = !std::is_arithmetic_v<std::remove_reference_t<T>> and not is_instantiation_of_expr_v<T>;

  template<typename _T_>
  auto map_for_printing(const _T_& _t)
  {
    return map_for_expr_printing<_T_>::map(_t);
  }

#define CLOG_INTERNAL_DEFINE_OPERATOR(op, name)                                                    \
  struct name                                                                                      \
  {                                                                                                \
    template<typename LhsT, typename RhsT>                                                         \
    auto operator()(const LhsT& _lhs, const RhsT& _rhs)                                            \
    {                                                                                              \
      return get_value(_lhs) op get_value(_rhs);                                                   \
    }                                                                                              \
    template<typename _T_>                                                                         \
      requires(not is_instantiation_of_expr_v<_T_>)                                                \
    _T_ get_value(const _T_& _t)                                                                   \
    {                                                                                              \
      return _t;                                                                                   \
    }                                                                                              \
    template<typename _T_>                                                                         \
      requires is_instantiation_of_expr_v<_T_>                                                     \
    auto get_value(const _T_& _t)                                                                  \
    {                                                                                              \
      return _t.value();                                                                           \
    }                                                                                              \
  };

  CLOG_INTERNAL_DEFINE_OPERATOR(&&, logical_and)
  CLOG_INTERNAL_DEFINE_OPERATOR(||, logical_or)

  CLOG_INTERNAL_DEFINE_OPERATOR(<, less)
  CLOG_INTERNAL_DEFINE_OPERATOR(>, greater)
  CLOG_INTERNAL_DEFINE_OPERATOR(<=, less_equal)
  CLOG_INTERNAL_DEFINE_OPERATOR(>=, greater_equal)
  CLOG_INTERNAL_DEFINE_OPERATOR(|, bit_or)
  CLOG_INTERNAL_DEFINE_OPERATOR(&, bit_and)
  CLOG_INTERNAL_DEFINE_OPERATOR(^, bit_xor)
#undef CLOG_INTERNAL_DEFINE_OPERATOR

  template<typename _T_>
  struct get_type
  {
    using type = _T_;
  };
  template<typename _T_>
  struct get_type<unary_expr<_T_>>
  {
    using type = _T_;
  };

  template<typename T>
  inline constexpr bool is_pointer_v
    = std::is_pointer_v<remove_cvref_t<typename get_type<T>::type>>;

  template<typename LhsT, typename RhsT, typename OpT>
  class binary_expr
  {
    LhsT m_lhs;
    const char* m_op;
    RhsT m_rhs;
    mutable bool m_result_computed = false;
    mutable bool m_result = false;
  public:
    template<typename FormatContext>
    auto format(FormatContext& ctx) const
    {
      return std::format_to(ctx.out(), "{} {} {}", map_for_printing(m_lhs), m_op,
                            map_for_printing(m_rhs));
    }
  public:
    binary_expr(LhsT _lhs, const char* _op, RhsT _rhs) : m_lhs(_lhs), m_op(_op), m_rhs(_rhs) {}

    bool result() const
    {
      if(not m_result_computed)
      {
        m_result_computed = true;
        m_result = OpT()(m_lhs, m_rhs);
      }
      return m_result;
    }
    bool value() const { return result(); }

#define CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(op, opf, is_and)                                  \
  template<typename LhsT_o, typename RhsT_o, typename OpT_o>                                       \
  auto operator op(const binary_expr<LhsT_o, RhsT_o, OpT_o>& _rhs)                                 \
    const->binary_expr<binary_expr<LhsT, RhsT, OpT>, binary_expr<LhsT_o, RhsT_o, OpT_o>, opf>      \
  {                                                                                                \
    return {*this, #op, _rhs};                                                                     \
  }                                                                                                \
  template<typename RhsT_o>                                                                        \
    requires is_not_arithmetic_and_not_expr_v<RhsT_o>                                              \
  friend auto operator op(binary_expr&& _lhs, RhsT_o&& _rhs)                                       \
    ->binary_expr<binary_expr, RhsT_o const&, opf>                                                 \
  {                                                                                                \
    return {_lhs, #op, _rhs};                                                                      \
  }                                                                                                \
  template<typename RhsT_o>                                                                        \
    requires std::is_arithmetic_v<RhsT_o>                                                          \
  friend auto operator op(binary_expr&& _lhs, RhsT_o _rhs)->binary_expr<binary_expr, RhsT_o, opf>  \
  {                                                                                                \
    return {_lhs, #op, _rhs};                                                                      \
  }                                                                                                \
  template<typename LhsT_o>                                                                        \
    requires is_not_arithmetic_and_not_expr_v<LhsT_o>                                              \
  friend auto operator op(LhsT_o&& _lhs, binary_expr&& _rhs)                                       \
    ->binary_expr<LhsT_o const&, binary_expr, opf>                                                 \
  {                                                                                                \
    return {_lhs, #op, _rhs};                                                                      \
  }                                                                                                \
  template<typename LhsT_o>                                                                        \
    requires std::is_arithmetic_v<LhsT_o>                                                          \
  friend auto operator op(LhsT_o _lhs, binary_expr&& _rhs)->binary_expr<LhsT_o, binary_expr, opf>  \
  {                                                                                                \
    static_assert(not is_and || not is_pointer_v<LhsT_o>,                                          \
                  "And comparison with pointer is not safe and not allowed. "                      \
                  "Wrap the expression inside parentheses, or use clog_assert_precond");           \
    return {_lhs, #op, _rhs};                                                                      \
  }

    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(&&, logical_and, true)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(||, logical_or, false)

#undef CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR

    template<typename T>
    auto operator==(T) const -> binary_expr const
    {
      static_assert(always_false<T>::value,
                    "chained comparisons are not supported inside assertions, "
                    "wrap the expression inside parentheses, or decompose it");
    }

    template<typename T>
    auto operator!=(T) const -> binary_expr const
    {
      static_assert(always_false<T>::value,
                    "chained comparisons are not supported inside assertions, "
                    "wrap the expression inside parentheses, or decompose it");
    }

    template<typename T>
    auto operator>(T) const -> binary_expr const
    {
      static_assert(always_false<T>::value,
                    "chained comparisons are not supported inside assertions, "
                    "wrap the expression inside parentheses, or decompose it");
    }

    template<typename T>
    auto operator<(T) const -> binary_expr const
    {
      static_assert(always_false<T>::value,
                    "chained comparisons are not supported inside assertions, "
                    "wrap the expression inside parentheses, or decompose it");
    }

    template<typename T>
    auto operator>=(T) const -> binary_expr const
    {
      static_assert(always_false<T>::value,
                    "chained comparisons are not supported inside assertions, "
                    "wrap the expression inside parentheses, or decompose it");
    }

    template<typename T>
    auto operator<=(T) const -> binary_expr const
    {
      static_assert(always_false<T>::value,
                    "chained comparisons are not supported inside assertions, "
                    "wrap the expression inside parentheses, or decompose it");
    }
  };

  // Specialised comparison functions to handle equality comparisons between ints and pointers (NULL
  // deduces as an int)

  struct compareEqual
  {

    template<typename LhsT, typename RhsT>
    auto operator()(LhsT const& lhs, RhsT const& rhs) -> bool
    {
      return static_cast<bool>(lhs == rhs);
    }
    template<typename T>
    auto operator()(T* const& lhs, int rhs) -> bool
    {
      return lhs == reinterpret_cast<void const*>(rhs);
    }
    template<typename T>
    auto operator()(T* const& lhs, long rhs) -> bool
    {
      return lhs == reinterpret_cast<void const*>(rhs);
    }
    template<typename T>
    auto operator()(int lhs, T* const& rhs) -> bool
    {
      return reinterpret_cast<void const*>(lhs) == rhs;
    }
    template<typename T>
    auto operator()(long lhs, T* const& rhs) -> bool
    {
      return reinterpret_cast<void const*>(lhs) == rhs;
    }
  };

  struct compareNotEqual
  {
    template<typename LhsT, typename RhsT>
    auto operator()(LhsT const& lhs, RhsT&& rhs) -> bool
    {
      return static_cast<bool>(lhs != rhs);
    }
    template<typename T>
    auto operator()(T* const& lhs, int rhs) -> bool
    {
      return lhs != reinterpret_cast<void const*>(rhs);
    }
    template<typename T>
    auto operator()(T* const& lhs, long rhs) -> bool
    {
      return lhs != reinterpret_cast<void const*>(rhs);
    }
    template<typename T>
    auto operator()(int lhs, T* const& rhs) -> bool
    {
      return reinterpret_cast<void const*>(lhs) != rhs;
    }
    template<typename T>
    auto operator()(long lhs, T* const& rhs) -> bool
    {
      return reinterpret_cast<void const*>(lhs) != rhs;
    }
  };

  template<typename T>
  class unary_expr
  {
    T m_lhs;
  public:
    static_assert(not is_instantiation_of<unary_expr, T>::value);
    explicit unary_expr(T lhs) : m_lhs(lhs) {}

    template<typename FormatContext>
    auto format(FormatContext& ctx) const
    {
      return std::formatter<remove_cvref_t<decltype(map_for_printing(m_lhs))>>().format(
        map_for_printing(m_lhs), ctx);
    }

#define CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(op, opf, is_and)                                  \
  template<typename RhsT>                                                                          \
    requires is_not_arithmetic_and_not_expr_v<RhsT>                                                \
  friend auto operator op(unary_expr&& lhs, RhsT&& rhs)->binary_expr<T, RhsT const&, opf>          \
  {                                                                                                \
    static_assert(not is_and || not is_pointer_v<T>,                                               \
                  "And comparison with pointer is not safe and not allowed. "                      \
                  "Wrap the expression inside parentheses, or use clog_assert_precond");           \
    return {lhs.value(), #op, rhs};                                                                \
  }                                                                                                \
  template<typename RhsT>                                                                          \
    requires std::is_arithmetic_v<RhsT>                                                            \
  friend auto operator op(unary_expr&& lhs, RhsT rhs)->binary_expr<T, RhsT, opf>                   \
  {                                                                                                \
    static_assert(not is_and || not is_pointer_v<T>,                                               \
                  "And comparison with pointer is not safe and not allowed. "                      \
                  "Wrap the expression inside parentheses, or use clog_assert_precond");           \
    return {lhs.value(), #op, rhs};                                                                \
  }                                                                                                \
  template<typename LhsT>                                                                          \
    requires(not std::is_arithmetic_v<std::remove_reference_t<LhsT>>)                              \
  friend auto operator op(LhsT&& lhs, unary_expr&& rhs)->binary_expr<LhsT const&, T, opf>          \
  {                                                                                                \
    return {lhs, #op, rhs.value()};                                                                \
  }                                                                                                \
  template<typename LhsT>                                                                          \
    requires std::is_arithmetic_v<LhsT>                                                            \
  friend auto operator op(LhsT lhs, unary_expr&& rhs)->binary_expr<LhsT, T, opf>                   \
  {                                                                                                \
    static_assert(not is_and || not is_pointer_v<LhsT>,                                            \
                  "And comparison with pointer is not safe and not allowed. "                      \
                  "Wrap the expression inside parentheses, or use clog_assert_precond");           \
    return {lhs, #op, rhs.value()};                                                                \
  }                                                                                                \
  template<typename LhsT>                                                                          \
  friend auto operator op(unary_expr<LhsT>&& lhs, unary_expr&& rhs)->binary_expr<LhsT, T, opf>     \
  {                                                                                                \
    static_assert(not is_and || not is_pointer_v<LhsT>,                                            \
                  "And comparison with pointer is not safe and not allowed. "                      \
                  "Wrap the expression inside parentheses, or use clog_assert_precond");           \
    return {lhs.value(), #op, rhs.value()};                                                        \
  }                                                                                                \
  template<typename LhsT, typename RhsT, typename OpT>                                             \
  friend auto operator op(binary_expr<LhsT, RhsT, OpT>&& lhs, unary_expr&& rhs)                    \
    ->binary_expr<binary_expr<LhsT, RhsT, OpT>, T, opf>                                            \
  {                                                                                                \
    return {lhs, #op, rhs.value()};                                                                \
  }                                                                                                \
  template<typename LhsT, typename RhsT, typename OpT>                                             \
  friend auto operator op(unary_expr&& lhs, binary_expr<LhsT, RhsT, OpT>&& rhs)                    \
    ->binary_expr<T, binary_expr<LhsT, RhsT, OpT>, opf>                                            \
  {                                                                                                \
    static_assert(not is_and || not is_pointer_v<T>,                                               \
                  "And comparison with pointer is not safe and not allowed. "                      \
                  "Wrap the expression inside parentheses, or use clog_assert_precond");           \
    return {lhs.value(), #op, rhs};                                                                \
  }

    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(==, compareEqual, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(!=, compareNotEqual, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(<, less, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(>, greater, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(<=, less_equal, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(>=, greater_equal, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(|, bit_or, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(&, bit_and, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(^, bit_xor, false)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(&&, logical_and, true)
    CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR(||, logical_or, false)

#undef CLOG_INTERNAL_DEFINE_EXPRESSION_OPERATOR

    bool result() const { return static_cast<bool>(m_lhs); }
    T value() const { return m_lhs; }
  };

  struct decomposer
  {
    template<typename T>
      requires(not std::is_arithmetic_v<std::remove_reference_t<T>>)
    friend auto operator<<(decomposer&&, T&& lhs) -> unary_expr<T const&>
    {
      return unary_expr<const T&>{lhs};
    }

    template<typename T>
      requires std::is_arithmetic_v<T>
    friend auto operator<<(decomposer&&, T value) -> unary_expr<T>
    {
      return unary_expr<T>{value};
    }
    template<typename T>
      requires is_not_arithmetic_and_not_expr_v<T>
    friend auto operator<<(T&& lhs, decomposer&&) -> unary_expr<T const&>
    {
      return unary_expr<const T&>{lhs};
    }
    template<typename T>
    friend auto operator<<(unary_expr<T>&& lhs, decomposer&&) -> unary_expr<T>
    {
      return lhs;
    }
    template<typename T>
      requires std::is_arithmetic_v<T>
    friend auto operator<<(T value, decomposer&&) -> unary_expr<T>
    {
      return unary_expr<T>{value};
    }
  };

  template<typename _TExpr_>
    requires is_instantiation_of_expr_v<_TExpr_>
  struct expression_result
  {
    friend struct std::formatter<clog_impl::expression_result<_TExpr_>>;
    expression_result(const char* _expr_str, _TExpr_&& _expr) : m_expr_str(_expr_str), m_expr(_expr)
    {
    }
    auto value() const { return m_expr.value(); }
  private:
    template<typename FormatContext>
    auto format(FormatContext& ctx) const
    {
      return std::format_to(ctx.out(), "{} ({} = {})", m_expr_str, m_expr.value(), m_expr);
    }

    const char* m_expr_str;
    _TExpr_ m_expr;
  };
} // namespace clog_impl

template<typename LhsT, typename RhsT, typename OpT>
struct std::formatter<clog_impl::binary_expr<LhsT, RhsT, OpT>> : public clog_format::base_formatter
{
  template<typename FormatContext>
  auto format(const clog_impl::binary_expr<LhsT, RhsT, OpT>& p, FormatContext& ctx) const
  {
    return p.format(ctx);
  }
};

template<typename LhsT>
struct std::formatter<clog_impl::unary_expr<LhsT>> : public clog_format::base_formatter
{
  template<typename FormatContext>
  auto format(const clog_impl::unary_expr<LhsT>& p, FormatContext& ctx) const
  {
    return p.format(ctx);
  }
};

template<typename _TExpr_>
struct std::formatter<clog_impl::expression_result<_TExpr_>> : public clog_format::base_formatter
{
  template<typename FormatContext>
  auto format(const clog_impl::expression_result<_TExpr_>& p, FormatContext& ctx) const
  {
    return p.format(ctx);
  }
};
