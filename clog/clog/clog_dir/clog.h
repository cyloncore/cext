#ifndef _CLOG_H_
#define _CLOG_H_

#include <fstream>
#include <string>

#define CLOG_ENABLED
#define CLOG_USES_STD_FORMAT

#ifdef __CRES_FAKE_CLOG_ENABLED
#error "Include clog header before cres."
#endif

#define __CLOG_INTERNAL_STRINGIFY(...) #__VA_ARGS__

namespace clog_impl
{
  void clog_report_error(const std::string& _source, const std::string& _msg);
}

#include "config.h"

#include "macros.h"

#include "level.h"

#include "abstract_logging_listener.h"
#include "stream_logging_listener.h"

#include "logging_manager.h"

#include "decomposer.h"
#include "handle_assert.h"
#include "logging.h"
#include "utils.h"

using clog_abstract_logging_listener = clog_impl::abstract_logging_listener;

inline void clog_stream_to_file(const std::string& _name)
{
  clog_impl::logging_manager::add_listener(
    new clog_impl::stream_logging_listener(new std::ofstream(_name), true));
}

inline void clog_remove_all_listener() { clog_impl::logging_manager::remove_all_listener(); }

using clog_level = clog_impl::level;

constexpr clog_impl::level clog_level_info = clog_impl::level::info;
constexpr clog_impl::level clog_level_debug = clog_impl::level::debug;
constexpr clog_impl::level clog_level_warning = clog_impl::level::warning;
constexpr clog_impl::level clog_level_error = clog_impl::level::error;
constexpr clog_impl::level clog_level_all = clog_impl::level::all;

inline void clog_disable(clog_level _level) { clog_impl::logging_manager::disable(_level); }

inline void clog_enable(clog_level _level) { clog_impl::logging_manager::enable(_level); }

inline bool clog_is_enabled(clog_level _level)
{
  return clog_impl::logging_manager::is_enabled(_level);
}

#ifdef CLOG_HAS_SQLITE

#include "sqlite_logging_listener.h"

inline void clog_stream_to_sqlite(const std::string& _name)
{
  clog_impl::logging_manager::add_listener(new clog_impl::sqlite_logging_listener(_name));
}

#endif

#ifdef CLOG_HAS_LOKI

#include "loki_logging_listener.h"

inline void clog_stream_to_loki(const std::string& _hostname, const std::string& _logger_name,
                                const std::unordered_map<std::string, std::string>& _extra_labels,
                                const std::string& _username, const std::string& _password)
{
  clog_impl::logging_manager::add_listener(new clog_impl::loki_logging_listener(
    _hostname, _logger_name, _extra_labels, _username, _password));
}

#endif
#endif
