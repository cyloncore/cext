
namespace clog_impl
{
  template<typename _TExpr_>
  inline void handle_assert(const char* _filename, int _line, const char* _string, _TExpr_ _expr)
  {
    if(not _expr.result())
    {
      clog_impl::logging_manager::report_fatal(_filename, _line, "assertion failed {} ({})",
                                               _string, _expr);
    }
  }
} // namespace clog_impl
