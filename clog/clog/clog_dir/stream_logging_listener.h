#ifndef _CLOG_H_
#include "abstract_logging_listener.h"
#endif

namespace clog_impl
{

  class stream_logging_listener : public abstract_logging_listener
  {
  public:
    stream_logging_listener(std::ostream* _stream, bool _own_stream);
    ~stream_logging_listener();
    void report_debug(const std::string& _filename, int _line,
                      const std::string& _message) override;
    void report_error(const std::string& _filename, int _line,
                      const std::string& _message) override;
    void report_info(const std::string& _filename, int _line, const std::string& _message) override;
    void report_fatal(const std::string& _filename, int _line,
                      const std::string& _message) override;
    void report_warning(const std::string& _filename, int _line,
                        const std::string& _message) override;
  private:
    struct data;
    data* d;
  };

} // namespace clog_impl
