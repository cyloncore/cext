#include <format>
#include <iostream>

namespace clog_impl
{
  class bench
  {
  public:
    bench();
    bench(const bench&) = delete;
    bench& operator=(const bench&) = delete;
    ~bench();
    void setup_columns(const std::string& _name);
    template<typename... _TARGS_>
    void setup_columns(const std::string& _name, const std::string& _name2, const _TARGS_&... _args)
    {
      setup_columns(_name);
      setup_columns(_name2, _args...);
    }
    template<typename _TM_, typename... _TARGS_>
    void start_iteration(const _TM_& _name, const _TARGS_&... _args)
    {
      start_iteration_impl(std::format("{}", _name));
      add_values(_args...);
    }
    template<typename... _TARGS_>
    void end_iteration(const _TARGS_&... _args)
    {
      end_iteration_impl();
      add_values(_args...);
      end_iteration_check();
    }
    template<typename _TM_, typename... _TARGS_>
    void add_values(const _TM_& _name, const _TARGS_&... _args)
    {
      add_value(std::format("{}", _name));
      add_values(_args...);
    }
    /**
     * Print results to a stream
     */
    void print_results(std::ostream& _stream, bool _show_header = false,
                       const std::string& _sep = " ") const;
    /**
     * Print results on the terminal
     */
    void print_results(bool _show_header = false, const std::string& _sep = " ") const
    {
      print_results(std::cout, _show_header, _sep);
    }
    /**
     * @return the current time
     */
    std::size_t current_time() const;
  private:
    void add_value(const std::string& _value);
    void add_values() {}
    void start_iteration_impl(const std::string& _name);
    void end_iteration_impl();
    void end_iteration_check();
    struct data;
    data* const d;
  };
} // namespace clog_impl

using clog_bench = clog_impl::bench;
