#ifndef _CLOG_LOGGING_H_
#define _CLOG_LOGGING_H_

#undef clog_assert
#undef clog_assert_msg

#define clog_error(...)                                                                            \
  if(clog_impl::logging_manager::should_report_errors(__FILE__))                                   \
  clog_impl::logging_manager::report_error(__FILE__, __LINE__, __VA_ARGS__)
#define clog_warning(...)                                                                          \
  if(clog_impl::logging_manager::should_report_warnings(__FILE__))                                 \
  clog_impl::logging_manager::report_warning(__FILE__, __LINE__, __VA_ARGS__)
#define clog_debug(...)                                                                            \
  if(clog_impl::logging_manager::should_report_debugs(__FILE__))                                   \
  clog_impl::logging_manager::report_debug(__FILE__, __LINE__, __VA_ARGS__)
#define clog_info(...)                                                                             \
  if(clog_impl::logging_manager::should_report_infos(__FILE__))                                    \
  clog_impl::logging_manager::report_info(__FILE__, __LINE__, __VA_ARGS__)
#define clog_fatal(...) clog_impl::logging_manager::report_fatal(__FILE__, __LINE__, __VA_ARGS__)

#define __clog_build_string_vn_indiv(X, I) " '{}' = {}"

#define __clog_build_string_vn(...) __clog_foreach(__clog_build_string_vn_indiv, __VA_ARGS__)

#define __clog_argumentify(X, I) #X, X,

#define __clog_build_args(...) __clog_foreach(__clog_argumentify, __VA_ARGS__)

#define clog_debug_vn(...)                                                                         \
  clog_debug(__clog_build_string_vn(__VA_ARGS__) "{}", __clog_build_args(__VA_ARGS__) "")

#define __clog_build_string_args_list_indiv(X, I) " {}"

#define __clog_build_string_args_list(...)                                                         \
  __clog_foreach(__clog_build_string_args_list_indiv, __VA_ARGS__)

#define clog_args_list(...) __clog_build_string_args_list(__VA_ARGS__) ""

/**
 * @code
 * int a = 2;
 * auto result = clog_eval_expr(a < 10);
 *
 * clog_debug("result = {}", result); // will print "a < 10 (2 < 10)"
 * if(result.value())
 * {
 *   // do something
 * }
 * @endcode
 */
#define clog_eval_expr(...)                                                                        \
  (cext_clang_pragma(diagnostic push)                                                              \
     cext_clang_pragma(diagnostic ignored "-Woverloaded-shift-op-parentheses")                     \
       clog_impl::expression_result(__CLOG_INTERNAL_STRINGIFY(__VA_ARGS__),                        \
                                    clog_impl::decomposer()                                        \
                                      << __VA_ARGS__ << clog_impl::decomposer())                   \
         cext_clang_pragma(diagnostic pop))

#define __CLOG_HAS_ASSERT 1

#ifdef NDEBUG
#if !defined(CLOG_FORCE_ASSERT)
#undef __CLOG_HAS_ASSERT
#define __CLOG_HAS_ASSERT 0
#endif
#endif

#if __CLOG_HAS_ASSERT

#define clog_assert(...)                                                                           \
  cext_clang_pragma(diagnostic push);                                                              \
  cext_clang_pragma(diagnostic ignored "-Woverloaded-shift-op-parentheses");                       \
  clog_impl::handle_assert(__FILE__, __LINE__, __CLOG_INTERNAL_STRINGIFY(__VA_ARGS__),             \
                           clog_impl::decomposer() << __VA_ARGS__ << clog_impl::decomposer());     \
  cext_clang_pragma(diagnostic pop);

#define clog_assert_precond(precond, ...)                                                          \
  clog_assert(precond);                                                                            \
  clog_assert(__VA_ARGS__);

#define clog_assert_msg(assrt, msg, ...)                                                           \
  if(not(assrt))                                                                                   \
  {                                                                                                \
    clog_fatal(msg, #assrt, __VA_ARGS__);                                                          \
  }
#define clog_near_equal_assert(v1, v2, tol)                                                        \
  {                                                                                                \
    auto __clog__nea_ev1_ = (v1);                                                                  \
    auto __clog__nea_ev2_ = (v2);                                                                  \
    auto __clog__nea_tol_ = (tol);                                                                 \
    auto __clog__nea_error_ = std::abs(__clog__nea_ev1_ - __clog__nea_ev2_);                       \
    if(__clog__nea_error_ > __clog__nea_tol_)                                                      \
    {                                                                                              \
      clog_fatal("Expected near equal: '%{}' != '%{}' error: '%{}', tolerance: '%{}'",             \
                 __clog__nea_ev1_, __clog__nea_ev2_, __clog__nea_error_, __clog__nea_tol_);        \
    }                                                                                              \
  }

#else

#define clog_assert(assrt)                                                                         \
  do                                                                                               \
  {                                                                                                \
  } while((false) && (assrt))
#define clog_assert_msg(assrt, msg, ...) clog_assert(assrt)
#define clog_near_equal_assert(v1, v2, tol)                                                        \
  (void)v1;                                                                                        \
  (void)v2;                                                                                        \
  (void)tol;                                                                                       \
  do                                                                                               \
  {                                                                                                \
  } while(false)

#endif

#endif
