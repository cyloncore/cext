#include "../clog"

namespace clog_impl
{
  void clog_report_error(const std::string& _source, const std::string& _msg)
  {
    clog_impl::logging_manager::report_error(_source, 0, _msg);
  }
} // namespace clog_impl
