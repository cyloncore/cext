#include <map>
#include <optional>
#include <sstream>
#include <unordered_map>
#include <vector>

namespace clog_format
{
  template<typename _T_>
  using is_formattable = std::is_default_constructible<std::formatter<_T_, char>>;
  template<typename _Tp>
  constexpr bool is_formattable_v = is_formattable<_Tp>::value;
  /**
   * Base for formatters with no formatting options.
   */
  struct base_formatter
  {
    template<class ParseContext>
    constexpr ParseContext::iterator parse(ParseContext& ctx)
    {
      auto it = ctx.begin(), end = ctx.end();

      if(it != end && *it != '}')
        throw std::format_error("invalid format");

      return it;
    }
  };
  /**
   * Base for formatters that will capture the format specification, can be usefull for forwarding
   * to an other formatter.
   */
  struct base_formatter_capture_specification : public base_formatter
  {
    template<class ParseContext>
    constexpr ParseContext::iterator parse(ParseContext& ctx)
    {
      for(auto it = ctx.begin(); it != ctx.end(); ++it)
      {
        if(*it == '}')
        {
          return it;
        }
        else
        {
          format_str += *it;
        }
      }
      return ctx.end();
    }
  protected:
    std::string get_format_str() const { return format_str; }
  private:
    std::string format_str;
  };

  struct base_container_formatter : public base_formatter
  {
    template<class ParseContext>
    constexpr ParseContext::iterator parse(ParseContext& ctx)
    {
      value_format = "{:";
      for(auto it = begin(ctx); it != end(ctx); ++it)
      {
        char c = *it;
        if(c == 'c')
        {
          curly = true;
        }
        else if(c == '}')
        {
          value_format += "}";
          return it;
        }
        else if(c == 's')
        {
          ++it;
          if(it == end(ctx))
          {
            throw std::format_error("Missing 'txt' after seperator.");
          }
          if(*it != '\'')
          {
            throw std::format_error("Expected ' after s.");
          }
          separator = std::string();
          ++it;
          for(; it != end(ctx); ++it)
          {
            if(*it == '\'')
            {
              break;
            }
            else
            {
              separator += *it;
            }
          }
        }
        else
        {
          value_format += c;
        }
      }
      return std::end(ctx);
    }
    bool curly{false};
    std::string value_format;
    std::string separator = ", ";
  };

  template<typename _T_>
  struct base_iterable_formatter : public base_container_formatter
  {
    template<typename FormatContext>
    FormatContext::iterator format(const _T_& p, FormatContext& ctx) const
    {
      typename FormatContext::iterator out = ctx.out();
      out = std::vformat_to(out, curly ? "{{" : "[", std::make_format_args());
      bool first = true;
      for(const auto& v : p)
      {
        if(first)
        {
          out = std::vformat_to(out, value_format, std::make_format_args(v));
          first = false;
        }
        else
        {
          out = std::vformat_to(out, separator + value_format, std::make_format_args(v));
        }
      }
      return vformat_to(out, curly ? "}}" : "]", std::make_format_args());
    }
  };

  /**
   * This class is used to access the iterator that return a key/value.
   * The default variant uses begin/end. See Qt formatters for an example of use.
   */
  template<typename _T_>
  class as_kv_range
  {
  public:
    as_kv_range(const _T_& data) : m_data{data} {}
    auto begin() { return m_data.begin(); }
    auto end() { return m_data.end(); }
  private:
    const _T_& m_data;
  };

  template<typename _T_>
  struct base_map_formatter : public clog_format::base_container_formatter
  {
    template<typename FormatContext>
    auto format(const _T_& p, FormatContext& ctx) const
    {
      auto&& out = ctx.out();
      out = std::vformat_to(out, curly ? "[" : "{{", std::make_format_args());
      bool first = true;
      for(const auto& [k, v] : as_kv_range<_T_>(p))
      {
        if(first)
        {
          out = std::vformat_to(out, "{}: " + value_format, std::make_format_args(k, v));
          first = false;
        }
        else
        {
          out = std::vformat_to(out, ", {}: " + value_format, std::make_format_args(k, v));
        }
      }
      return std::vformat_to(out, curly ? "]" : "}}", std::make_format_args());
    }
  };

  template<typename Char>
  struct base_ostream_formatter : public base_container_formatter
  {
    template<typename T, typename FormatContext>
    FormatContext::iterator format(const T& value, FormatContext& ctx) const
    {
      std::basic_stringstream<Char> ss;
      ss << value;
      return std::format_to(ctx.out(), "{}", ss.view());
    }
  };

  /**
   * Define a formatter when formatting is done by casting to an other type.
   */
  template<typename _FROM_TYPE_, typename _TO_TYPE_>
  struct cast_formatter : public std::formatter<_TO_TYPE_>
  {
    template<typename FormatContext>
    FormatContext::iterator format(const _FROM_TYPE_& p, FormatContext& ctx) const
    {
      return std::formatter<_TO_TYPE_>::format(p, ctx);
    }
  };
} // namespace clog_format

template<typename _T_>
struct std::formatter<std::optional<_T_>> : public std::formatter<_T_>
{
  template<typename FormatContext>
  auto format(const std::optional<_T_>& p, FormatContext& ctx) const
  {
    if(p)
    {
      return std::formatter<_T_>::format(*p, ctx);
    }
    else
    {
      return std::format_to(ctx.out(), "null");
    }
  }
};

template<typename _T_>
  requires clog_format::is_formattable_v<_T_>
struct std::formatter<std::vector<_T_>>
    : public clog_format::base_iterable_formatter<std::vector<_T_>>
{
};

template<typename _T_, std::size_t _Nm_>
  requires clog_format::is_formattable_v<_T_>
struct std::formatter<std::array<_T_, _Nm_>>
    : public clog_format::base_iterable_formatter<std::array<_T_, _Nm_>>
{
};

template<typename _T1_, typename _T2_>
struct std::formatter<std::pair<_T1_, _T2_>> : public clog_format::base_formatter
{
  template<typename FormatContext>
  FormatContext::iterator format(const std::pair<_T1_, _T2_>& p, FormatContext& ctx) const
  {
    return std::format_to(ctx.out(), "{}: {}", p.first, p.second);
  }
};

template<typename _K_, typename _T_>
  requires clog_format::is_formattable_v<_K_> and clog_format::is_formattable_v<_T_>
struct std::formatter<std::map<_K_, _T_>>
    : public clog_format::base_map_formatter<std::map<_K_, _T_>>
{
};

template<typename _K_, typename _T_>
  requires clog_format::is_formattable_v<_K_> and clog_format::is_formattable_v<_T_>
struct std::formatter<std::unordered_map<_K_, _T_>>
    : public clog_format::base_map_formatter<std::unordered_map<_K_, _T_>>
{
};

#define __clog_format_declare_enum_formatter_case(_VALUE_, _I_)                                    \
  case enum_type::_VALUE_:                                                                         \
    return std::format_to(ctx.out(), "{}", #_VALUE_);

/**
 * Convenient macro for creating a formatter for enums.
 *
 * @code
 * enum class MyEnum { Value1, Value2, Value3 };
 *
 * clog_format_declare_enum_formatter(MyEnum, Value1, Value2, Value3);
 * @endcode
 */
#define clog_format_declare_enum_formatter(_TYPE_, ...)                                            \
  template<>                                                                                       \
  struct std::formatter<_TYPE_> : public clog_format::base_formatter                               \
  {                                                                                                \
    template<typename FormatContext>                                                               \
    FormatContext::iterator format(const _TYPE_& p, FormatContext& ctx) const                      \
    {                                                                                              \
      using enum_type = _TYPE_;                                                                    \
      switch(p)                                                                                    \
      {                                                                                            \
        __clog_foreach(__clog_format_declare_enum_formatter_case, __VA_ARGS__);                    \
      }                                                                                            \
      return std::format_to(ctx.out(), "Unhandled enum value {}", (int)p);                         \
    }                                                                                              \
  }

/**
 * Convenient macro for declaring formatters
 *
 * @code
 * struct MyStruct { int a, b; };
 * clog_format_declare_formatter(scQL::Core::SymbolicObject::Field)
 * {
 *   return std::format_to(ctx.out(), "({}, {})", p.a, p.b);
 * }
 * @endcode
 */
#define clog_format_declare_formatter(_TYPE_, ...)                                                 \
  template<>                                                                                       \
  struct std::formatter<_TYPE_>                                                                    \
      : public __clog_select(__VA_ARGS__ __VA_OPT__(, ) clog_format::base_formatter)               \
  {                                                                                                \
    template<typename FormatContext>                                                               \
    FormatContext::iterator format(_TYPE_ const& p, FormatContext& ctx) const;                     \
  };                                                                                               \
  template<typename FormatContext>                                                                 \
  FormatContext::iterator std::formatter<_TYPE_>::format(_TYPE_ const& p, FormatContext& ctx)      \
    const

/**
 * Convenient macro for declaring formatters which inherits an other one.
 *
 * @code
 * struct MyStruct { std::string a; };
 * clog_format_declare_formatter_inherit(MyStruct, std::string)
 * {
 *   return super::format(p.a, ctx);
 * }
 * @endcode
 */
#define clog_format_declare_formatter_inherit(_TYPE_, _OTHER_T_)                                   \
  template<>                                                                                       \
  struct std::formatter<_TYPE_> : public std::formatter<_OTHER_T_>                                 \
  {                                                                                                \
    using super = std::formatter<_OTHER_T_>;                                                       \
    template<typename FormatContext>                                                               \
    FormatContext::iterator format(_TYPE_ const& p, FormatContext& ctx) const;                     \
  };                                                                                               \
  template<typename FormatContext>                                                                 \
  FormatContext::iterator std::formatter<_TYPE_>::format(_TYPE_ const& p, FormatContext& ctx)      \
    const

#define clog_format_declare_cast_formatter(_TYPE_, _TO_TYPE_)                                      \
  template<>                                                                                       \
  struct std::formatter<_TYPE_> : public clog_format::cast_formatter<_TYPE_, _TO_TYPE_>            \
  {                                                                                                \
  }
