
namespace clog_format
{
  template<typename _T_>
  std::string to_string(const _T_& _v)
  {
    return std::format("{}", _v);
  }

  template<typename _Out_, typename _T_>
  _Out_ to_string(_Out_ _out, const _T_& _v)
  {
    return std::format_to(_out, "{}", _v);
  }
} // namespace clog_format
