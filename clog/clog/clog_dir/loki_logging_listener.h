#ifndef _CLOG_H_
#include "abstract_logging_listener.h"
#endif

#include <unordered_map>

namespace clog_impl
{
  class loki_logging_listener : public abstract_logging_listener
  {
  public:
    loki_logging_listener(const std::string& _hostname, const std::string& _logger_name,
                          const std::unordered_map<std::string, std::string>& _extra_labels,
                          const std::string& _username, const std::string& _password);
    ~loki_logging_listener();
    void report_debug(const std::string& _filename, int _line,
                      const std::string& _message) override;
    void report_error(const std::string& _filename, int _line,
                      const std::string& _message) override;
    void report_info(const std::string& _filename, int _line, const std::string& _message) override;
    void report_fatal(const std::string& _filename, int _line,
                      const std::string& _message) override;
    void report_warning(const std::string& _filename, int _line,
                        const std::string& _message) override;
  private:
    struct data;
    data* d;
  };
} // namespace clog_impl
