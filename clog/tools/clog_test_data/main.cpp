#include <clog>

int main(int, char**)
{
#ifdef CLOG_HAS_SQLITE
  clog_stream_to_sqlite("test.clog");
#endif
#ifdef CLOG_HAS_LOKI
  clog_stream_to_loki("http://localhost:3100/", "clog_test_data", {}, {}, {});
#endif

  clog_debug("This a debug message.");
  clog_info("This an information message.");
  clog_error("This an error message.");
  return 0;
}

// curl -X  POST -H  "Content-Type: application/json" "http://localhost:3102/loki/api/v1/push" -d "{
//           \"streams\": [
//             {
//               \"stream\": {
//                 \"severity\": \"error\",
//                 \"logger\": \"clog_test_data\",
//                 \"filename\": \"/home/cyrille/cc-pkg/src/clog/tools/clog_test_data/main.cpp\",
//                 \"line\": 14

//             },
//               \"values\": [[1716650450685283725, \"This an error message.\"]]
//            }
//           ]
//         }"