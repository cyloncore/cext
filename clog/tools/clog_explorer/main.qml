import QtQuick.Controls
import QtQuick.Layouts

import Cyqlops.Controls
import Cyqlops.DB.SQLite

ApplicationWindow
{
  visible: true
  Database
  {
    id: _sqlite_db_
  }
  Query
  {
    id: _sqlite_query_
    database: _sqlite_db_
  }
  GridLayout
  {
    anchors.fill: parent
    columns: 3
    Label
    {
      text: "Filename:"
    }
    TextField
    {
      id: _filename_
      text: "/home/cyrille/test_clog"
      Layout.fillWidth: true
    }
    Button
    {
      text: "open"
      onClicked:
      {
        _sqlite_db_.filename = _filename_.text
        _sqlite_db_.open()
        if(_sqlite_query_.exec("SELECT level, timesamp, filename, line, message FROM clog_logs"))
        {
          console.log(_sqlite_query_.first())
        } else {
          console.log(_sqlite_query_.lastError)
        }
      }
    }
    TableView
    {
      Layout.fillHeight: true
      Layout.fillWidth: true
      Layout.columnSpan: 3
    }
  }
}
