#ifdef CLOG_ENABLED

#define cres_assert(X) clog_assert(X)
#define cres_fatal(...) clog_fatal(__VA_ARGS__)

#else

#include <assert.h>

#define cres_assert(X)                                                                             \
  assert(X);                                                                                       \
  do                                                                                               \
  {                                                                                                \
  } while((false) && (X))
#define cres_fatal(...) std::abort()

#define __CRES_FAKE_CLOG_ENABLED

#endif
