
#define __cres_cat(a, b) __cres_cat_I(a, b)
#define __cres_cat_I(a, b) __cres_cat_II(~, a##b)
#define __cres_cat_II(p, res) res

namespace cres::details
{
  struct Action
  {
    std::function<void()> act;
  };
  inline void execute_action(const Action& _act) { _act.act(); }
  template<typename... _T_>
  inline void execute_action(const _T_&...)
  {
  }
  /**
   * @internal
   * Use by cres_try to switch _err and _format when an optional argument is passed
   */
  template<typename _E_>
    requires(cres::error_traits<_E_>::value)
  inline const _E_& error_vta(const _E_& _err)
  {
    return _err;
  }
  template<typename _E_>
    requires(cres::error_traits<_E_>::value)
  inline const _E_& error_vta(const _E_& _err, const Action&)
  {
    return _err;
  }
  /**
   * @internal
   * Use by cres_try to switch _err and _format when an optional argument is passed
   */
  template<typename _E_, typename... _TArgs_>
    requires(cres::error_traits<_E_>::value)
  inline cres::error<typename _E_::string_type>
    error_vta(const _E_& _err, const Action&,
              const std::format_string<_TArgs_..., const _E_>& _format, _TArgs_&&... _args)
  {
    return cres_failure(_format, std::forward<_TArgs_>(_args)..., std::forward<const _E_>(_err));
  }
  /**
   * @internal
   * Use by cres_try to switch _err and _format when an optional argument is passed
   */
  template<typename _E_, typename... _TArgs_>
    requires(cres::error_traits<_E_>::value)
  inline cres::error<typename _E_::string_type>
    error_vta(const _E_& _err, const std::format_string<_TArgs_..., const _E_>& _format,
              _TArgs_&&... _args)
  {
    return cres_failure(_format, std::forward<_TArgs_>(_args)..., std::forward<const _E_>(_err));
  }
  /**
   * @internal
   * Used by cres to mark a retrurned value as ignored
   */
  struct ignore
  {
    template<typename _T_>
    ignore& operator=(const _T_&)
    {
      return *this;
    }
  };
} // namespace cres::details

#define cres_ignore cres::details::ignore()

#define __cres_on_failure(...)                                                                     \
  cres::details::Action                                                                            \
  {                                                                                                \
    [&]() { __VA_ARGS__; }                                                                         \
  }

#define __cres_none nullptr

#define __cres_execute_action_(_ACTION_, ...) cres::details::execute_action(__cres_##_ACTION_);

#define __cres_message(...) __VA_ARGS__

#define __cres_value_format__0()

#define __cres_value_format__1(_ACTION_OR_FORMAT_) __cres_##_ACTION_OR_FORMAT_

#define __cres_value_format__2(_ACTION_, _FORMAT_) __cres_##_FORMAT_

#define __cres_value_format_GET(_0, _1, _2, NAME, ...) NAME

#define __cres_value_format(...)                                                                   \
  __cres_value_format_GET(void, __VA_ARGS__ __VA_OPT__(, ) __cres_value_format__2,                 \
                          __cres_value_format__1, __cres_value_format__0)(__VA_ARGS__)

#define __cres_result_variable_name(_IDX_) __cres_cat(__krv__, _IDX_)

#define __cres_try_void(_IDX_, _EXPR_, ...)                                                        \
  auto __cres_result_variable_name(_IDX_) = (_EXPR_);                                              \
  if(not __cres_result_variable_name(_IDX_).is_successful())                                       \
  {                                                                                                \
    __cres_execute_action_(__VA_ARGS__ __VA_OPT__(, ) none);                                       \
    return cres::details::error_vta(__cres_result_variable_name(_IDX_).get_error() __VA_OPT__(, )  \
                                      __cres_value_format(__VA_ARGS__));                           \
  }

#define __cres_try(_IDX_, _VARIABLE_, _EXPR_, ...)                                                 \
  __cres_try_void(_IDX_, _EXPR_, __VA_ARGS__) _VARIABLE_                                           \
    = std::move(__cres_result_variable_name(_IDX_).move_value().value())

/**
 * Attempt to assign the result of expression \p _EXPR_ to \p _VARIABLE_.
 * If \p _EXPR_ fails, it returns the error message.
 *
 * \code
 * int variable_name;
 * cres_try(,variable_name, call_expr(...));
 * cres_try(int other_variable_name, call_expr(...));
 * cres_try(int other_variable_name, call_expr(...), on_failure({ dosomething_when_it_failed(); }));
 * \endcode
 */
#define cres_try(_VARIABLE_, _EXPR_, ...) __cres_try(__COUNTER__, _VARIABLE_, _EXPR_, __VA_ARGS__)

/**
 * If \p _CHECK_ is false, return the error message \p _ERROR_MSG_.
 */
#define cres_check(_CHECK_, _ERROR_MSG_, ...)                                                      \
  if(not(_CHECK_))                                                                                 \
  {                                                                                                \
    return cres_failure(_ERROR_MSG_ __VA_OPT__(, ) __VA_ARGS__);                                   \
  }
