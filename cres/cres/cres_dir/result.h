#include <format>

#include <type_traits>

namespace cres
{
  /**
   * @ingroup cres
   *
   * This class represent a void value. It is mainly intended to simplify the API
   * for function that returns a cres_result<void>.
   */
  struct void_type
  {
    template<typename _T_, typename _E_>
    friend class result;
    template<typename _T_>
    friend class std::optional;
    void_type() {}
    // private:
    void_type(const void_type&) {}
    void_type& operator=(const void_type&) { return *this; }
  };
  namespace details
  {
    template<typename _T_>
    struct result_traits
    {
      using value_type = _T_;
      static constexpr bool is_void = false;
      static const _T_& default_value()
      {
        static _T_ v;
        return v;
      }
    };
    template<>
    struct result_traits<void>
    {
      using value_type = void_type;
      static constexpr bool is_void = true;
      static const void_type& default_value()
      {
        static void_type v;
        return v;
      }
    };
    template<typename _T_>
    struct is_result : public std::false_type
    {
    };
    template<typename _T_, typename _E_>
    struct is_result<result<_T_, _E_>> : public std::true_type
    {
    };
  } // namespace details
} // namespace cres

template<typename _T_>
  requires(not std::is_same_v<_T_, void>)
cres::result<_T_, cres::no_error> cres_success(const _T_& _t);

cres::result<void, cres::no_error> cres_success();

namespace cres
{
  /**
   * @ingroup cres
   * Allows to return a value and a status (success or failure) and an error
   * message in case of failure.
   */
  template<typename _T_, typename _E_>
  class result
  {
    static_assert(not details::is_result<_T_>::value);
    using result_traits = details::result_traits<_T_>;
    using value_type = result_traits::value_type;
    template<typename _TOther_>
      requires(not std::is_same_v<_TOther_, void>)
    friend cres::result<_TOther_, cres::no_error>(::cres_success(const _TOther_& _t));

    friend result<void, no_error>(::cres_success());
  private:
    explicit result(const value_type& _value)
      requires(not std::is_same_v<value_type, void_type>)
        : m_success(true), m_value(_value)
    {
    }
    explicit result(const void_type&)
      requires std::is_same_v<value_type, void_type>
        : m_success(true), m_value(void_type{})
    {
    }
  public:
    result() : m_success(false), m_error(error_traits<_E_>::invalid()) {}
    result(const result& _rhs)
        : m_success(_rhs.m_success), m_value(_rhs.m_value), m_error(_rhs.m_error)
    {
      cres_assert((not m_error.has_value() or m_error.value() != error_traits<_E_>::invalid()));
    }
    result& operator=(const result& _rhs)
    {
      m_success = _rhs.m_success;
      m_value = _rhs.m_value;
      m_error = _rhs.m_error;
      cres_assert((not m_error.has_value() or m_error.value() != error_traits<_E_>::invalid()));
      return *this;
    }
    template<typename _TOther_>
      requires(not std::is_same_v<_E_, no_error> and std::is_convertible_v<_TOther_, _T_>
               and not std::is_same_v<_T_, void>)
    result(result<_TOther_, no_error>&& _rhs)
        : m_success(true), m_value(_T_(std::move(_rhs.move_value().value())))
    {
      cres_assert(_rhs.is_successful());
    }
    template<typename _TOther_>
      requires(not std::is_same_v<_E_, no_error> and std::is_convertible_v<_TOther_, _T_>
               and not std::is_same_v<_T_, void>)
    result(const result<_TOther_, no_error>& _rhs) : m_success(true), m_value(_T_(_rhs.get_value()))
    {
      cres_assert(_rhs.is_successful());
    }
    template<typename _TOther_>
      requires(not std::is_same_v<_E_, no_error> and std::is_same_v<_T_, void>
               and std::is_same_v<_TOther_, void>)
    result(const result<_TOther_, no_error>& _rhs) : m_success(true), m_value(void_type{})
    {
      cres_assert(_rhs.is_successful());
    }
    template<typename _TOther_>
      requires(std::is_convertible_v<_TOther_, _T_> and not std::is_same_v<_T_, _TOther_>)
    result(const result<_TOther_, _E_>& _rhs)
        : m_success(_rhs.is_successful()), m_value(_rhs.get_value()), m_error(_rhs.get_error())
    {
    }
    template<typename _TOther_, typename _OtherE_>
      requires(std::is_convertible_v<_TOther_, _T_> and std::is_convertible_v<_OtherE_, _E_>)
    result(result<_TOther_, _OtherE_>&& _rhs)
        : m_success(_rhs.is_successful()), m_value(_rhs.move_value()), m_error(_rhs.move_error())
    {
    }
    template<typename _OtherE_>
      requires(std::is_convertible_v<_OtherE_, _E_> and error_traits<_OtherE_>::value)
    result(const _OtherE_& _e) : m_success(false), m_error(_e)
    {
    }
    bool is_successful() const { return m_success; }
    const value_type& get_value() const
    {
      return m_value.has_value() ? m_value.value() : result_traits::default_value();
    }
    std::optional<value_type>&& move_value() { return std::move(m_value); }
    value_type&& expect_success()
    {
      if(not m_success)
      {
        cres_fatal("Success was expected but got error '{}'", get_error());
      }
      return std::move(m_value.value());
    }
    const _E_& get_error() const
    {
      return m_error.has_value() ? m_error.value() : error_traits<_E_>::no_error();
    }
    std::optional<_E_>&& move_error() { return std::move(m_error); }
    result<void, _E_> discard_value() const;
    // tuple API
    template<size_t I>
      requires(not result_traits::is_void)
    auto const& get() const&
    {
      if constexpr(I == 0)
        return m_success;
      else if constexpr(I == 1)
        return get_value();
      else if constexpr(I == 2)
        return get_error();
    }
    template<size_t I>
      requires(not result_traits::is_void)
    auto&& get() &&
    {
      if constexpr(I == 0)
        return std::move(m_success);
      else if constexpr(I == 1)
        return move_value();
      else if constexpr(I == 2)
        return move_error();
    }
    template<size_t I>
      requires result_traits::is_void
    auto const& get() const&
    {
      if constexpr(I == 0)
        return m_success;
      else if constexpr(I == 1)
        return get_error();
    }
    template<size_t I>
      requires result_traits::is_void
    auto&& get() &&
    {
      if constexpr(I == 0)
        return std::move(m_success);
      else if constexpr(I == 1)
        return move_error();
    }
  public:
    _T_ ok_or(const value_type& _v)
      requires(not result_traits::is_void)
    {
      return ok_or([&_v]() { return _v; });
    }
    _T_ ok_or(const std::function<_T_()>& _v)
      requires(not result_traits::is_void)
    {
      return m_success ? m_value.value() : _v();
    }
  private:
    bool m_success;
    std::optional<value_type> m_value;
    std::optional<_E_> m_error;
  };

} // namespace cres

template<typename _T_, typename _E_>
struct std::formatter<cres::result<_T_, _E_>> : public std::formatter<_T_>
{
  template<typename FormatContext>
  FormatContext::iterator format(cres::result<_T_, _E_> const& p, FormatContext& ctx) const
  {
    if(p.is_successful())
    {
      return std::formatter<_T_>::format(p.get_value(), ctx);
    }
    else
    {
      return format_to(ctx.out(), "failure({})", p.get_error());
    }
  }
};

template<typename _E_>
struct std::formatter<cres::result<void, _E_>>
{
  template<typename FormatContext>
  FormatContext::iterator format(cres::result<void, _E_> const& p, FormatContext& ctx) const
  {
    if(p.is_successful())
      return format_to(ctx.out(), "success()");
    else
      return format_to(ctx.out(), "failure({})", p.get_error());
  }
};

namespace std
{
  template<typename _T_, typename _E_>
  struct tuple_size<cres::result<_T_, _E_>> : std::integral_constant<size_t, 3>
  {
  };

  template<typename _T_, typename _E_>
  struct tuple_element<0, cres::result<_T_, _E_>>
  {
    using type = bool;
  };
  template<typename _T_, typename _E_>
  struct tuple_element<1, cres::result<_T_, _E_>>
  {
    using type = std::optional<typename cres::details::result_traits<_T_>::value_type>;
  };
  template<typename _T_, typename _E_>
  struct tuple_element<2, cres::result<_T_, _E_>>
  {
    using type = std::optional<_E_>;
  };

  template<typename _E_>
  struct tuple_size<cres::result<void, _E_>> : std::integral_constant<size_t, 2>
  {
  };

  template<typename _E_>
  struct tuple_element<0, cres::result<void, _E_>>
  {
    using type = bool;
  };
  template<typename _E_>
  struct tuple_element<1, cres::result<void, _E_>>
  {
    using type = std::optional<_E_>;
  };
} // namespace std

/**
 * Call this function to return from a function, after it was
 * successfull and should be returning \param _t.
 */
template<typename _T_>
  requires(not std::is_same_v<_T_, void>)
inline cres::result<_T_, cres::no_error> cres_success(const _T_& _t)
{
  return cres::result<_T_, cres::no_error>(_t);
}

/**
 * Call this function to return from a function, after it was
 * successfull and should be returning \ref cres_qresult<void>.
 */
inline cres::result<void, cres::no_error> cres_success()
{
  return cres::result<void, cres::no_error>(cres::void_type{});
}

/**
 * Call this function to return the results from an other function, which may
 * return a \ref result or may return a \p _T_.
 */
template<typename _T_, typename _E_>
inline cres::result<_T_, _E_> cres_return(const _T_& _t)
{
  return cres_success(_t);
}

template<typename _T_, typename _E_>
const inline cres::result<_T_, _E_>& cres_return(const cres::result<_T_, _E_>& _t)
{
  return _t;
}

template<typename _T_, typename _E_>
const inline cres::result<_T_, _E_>& cresReturn(const cres::result<_T_, _E_>& _t)
{
  return _t;
}

/**
 * Call this function to return an error from a function returning
 * \ref cres::result<_T_, cres::error>.
 *
 * @code
 *  int code = 10;
 *  return cres_failure("An error has occured with code '{}'", code);
 * @endcode
 */
template<typename... _Args_>
inline cres::error<std::string> cres_failure(std::format_string<_Args_...> _format,
                                             _Args_&&... _args)
{
  return cres::error<std::string>(std::vformat(_format.get(), std::make_format_args(_args...)));
}

inline cres::error<std::string> cres_failure(const char* _message)
{
  return cres::error<std::string>(_message);
}

template<typename _E_>
  requires(cres::error_traits<_E_>::value)
_E_ cres_forward_failure(const _E_& _error)
{
  return _error;
}

template<typename _E_>
  requires(cres::error_traits<_E_>::value)
_E_ cres_forward_failure(const std::optional<_E_>& _error)
{
  return _error.value();
}

template<typename _T_, typename _E_>
bool operator==(const cres::result<_T_, _E_>& _lhs, const _T_& _rhs)
{
  return _lhs.is_successful() and _lhs.get_value() == _rhs;
}

namespace cres::details
{
  template<typename _T_, typename _E_, typename... _Args_>
  struct cond
  {
    result<_T_, _E_> operator()(bool _cond, const _T_& _value,
                                std::format_string<_Args_...> _format, _Args_&&... _args)
    {
      if(_cond)
      {
        return cres_success(_value);
      }
      else
      {
        return cres_failure(_format, std::forward<_Args_...>(_args)...);
      }
    }
  };
  template<typename _E_, typename... _Args_>
  struct cond<void, _E_, _Args_...>
  {
    result<void, _E_> operator()(bool _cond, std::format_string<_Args_...> _format,
                                 _Args_&&... _args)
    {
      if(_cond)
      {
        return cres_success();
      }
      else
      {
        return cres_failure(_format, std::forward<_Args_...>(_args)...);
      }
    }
  };
} // namespace cres::details

template<typename _T_, typename... _Args_>
  requires(not std::is_same_v<_T_, void>)
inline cres::result<_T_, cres::error<std::string>>
  cres_cond(bool _cond, const _T_& _value, std::format_string<_Args_...> _format, _Args_&&... _args)
{
  return cres::details::cond<_T_, cres::error<std::string>, _Args_...>()(
    _cond, _value, _format, std::forward<_Args_...>(_args)...);
}

template<typename _T_, typename... _Args_>
  requires(std::is_same_v<_T_, void>)
inline cres::result<_T_, cres::error<std::string>>
  cres_cond(bool _cond, std::format_string<_Args_...> _format, _Args_&&... _args)
{
  static_assert(std::is_same_v<_T_, void>, "For non void, a value must be given");
  return cres::details::cond<_T_, cres::error<std::string>, _Args_...>()(
    _cond, _format, std::forward<_Args_...>(_args)...);
}

/**
 * @ingroup cext
 *
 * Function that try to assign the value in \param _return_value
 * to \param _value if \param _return_value was successfull.
 */
template<typename _TA_, typename _TRV_, typename _E_>
  requires(std::is_convertible_v<_TRV_, _TA_>)
inline cres::result<void, _E_> cres_try_assign(_TA_* _value,
                                               const cres::result<_TRV_, _E_>& _return_value)
{
  auto const [success, value, message] = _return_value;
  if(success)
  {
    *_value = value.value();
    return cres_success();
  }
  else
  {
    return message.value();
  }
}

template<typename _T_, typename _E_>
cres::result<void, _E_> cres::result<_T_, _E_>::discard_value() const
{
  return m_success ? cres_success() : cres::result<void, _E_>{*m_error};
}
