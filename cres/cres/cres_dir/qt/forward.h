class QString;

using cres_qerror = cres::error<QString>;

template<typename _T_, typename _E_ = cres_qerror>
using cres_qresult = cres::result<_T_, _E_>;
