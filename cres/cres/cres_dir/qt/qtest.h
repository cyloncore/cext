#pragma once

#include <QtTest/QtTest>

#define CRES_QVERIFY(_EXPR_)                                                                       \
  (                                                                                                \
    [&]()                                                                                          \
    {                                                                                              \
      auto __result__ = (_EXPR_);                                                                  \
      QTest::qVerify(__result__.is_successful(), #_EXPR_,                                          \
                     qPrintable(__result__.get_error().get_message()), __FILE__, __LINE__);        \
      if(not __result__.is_successful())                                                           \
      {                                                                                            \
        throw(std::runtime_error(__result__.get_error().get_message().toStdString()));             \
      }                                                                                            \
      return std::move(__result__.move_value().value());                                           \
    })()

#define CRES_QVERIFY_FAILURE(_EXPR_)                                                               \
  do                                                                                               \
  {                                                                                                \
    auto __result__ = (_EXPR_);                                                                    \
    if(!QTest::qVerify(static_cast<bool>(not __result__.is_successful()), #_EXPR_,                 \
                       qPrintable(__result__.get_error().get_message()), __FILE__, __LINE__))      \
      return;                                                                                      \
  } while(false)

#define CRES_QCOMPARE(_EXPR_, _EXPECTED_)                                                          \
  do                                                                                               \
  {                                                                                                \
    auto __result__ = (_EXPR_);                                                                    \
    if(!QTest::qVerify(static_cast<bool>(__result__.is_successful()), #_EXPR_,                     \
                       qPrintable(__result__.get_error().get_message()), __FILE__, __LINE__))      \
      return;                                                                                      \
    if(!QTest::qCompare(__result__.move_value().value(), _EXPECTED_, #_EXPR_, #_EXPECTED_,         \
                        __FILE__, __LINE__))                                                       \
      return;                                                                                      \
  } while(false)
