#include <QString>

template<>
struct cres::string_traits<QString>
{
  template<typename _TOtherString_>
  static QString from(const _TOtherString_& _string);
};

template<>
inline QString cres::string_traits<QString>::from<std::string>(const std::string& _string)
{
  return QString::fromStdString(_string);
}

inline cres_qerror cres_failure(const QString& _message) { return _message; }
