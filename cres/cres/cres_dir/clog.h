template<typename... _Args_>
inline cres::error<std::string> cres_log_failure(std::format_string<_Args_...> _format,
                                                 _Args_&&... _args)
{
  std::string str = std::vformat(_format.get(), std::make_format_args(_args...));
  clog_error(str);
  return cres::error<std::string>(str);
}

inline cres::error<std::string> cres_log_failure(const char* _message)
{
  clog_error(_message);
  return cres::error<std::string>(_message);
}
