namespace cres
{
  /**
   * @ingroup cres
   *
   * Defines a default error.
   */
  template<typename _TString_>
  class error
  {
  public:
    using string_type = _TString_;
  public:
    error() {}
    error(const _TString_& _message) : m_message(_message) {}
    template<typename _TOtherString_>
      requires(not std::is_same_v<_TString_, _TOtherString_>)
    error(const error<_TOtherString_>& _error)
        : m_message(string_traits<_TString_>::template from<_TOtherString_>(_error.get_message()))
    {
    }

    _TString_ get_message() const { return m_message; }
    bool operator==(const error& _error) const { return m_message == _error.m_message; }
    bool operator!=(const error& _error) const { return m_message != _error.m_message; }
  private:
    _TString_ m_message;
  };
  class no_error
  {
  };
  template<typename>
  struct error_traits : public std::false_type
  {
  };

  template<typename _TString_>
  struct error_traits<error<_TString_>> : public std::true_type
  {
    static const error<_TString_>& invalid()
    {
      static error<_TString_> invalid_error("Error is invalid and was not set properly.");
      return invalid_error;
    }
    static const error<_TString_>& no_error()
    {
      static error<_TString_> no_error("No error.");
      return no_error;
    }
  };
  template<>
  struct string_traits<std::string>
  {
    static const std::string& from(const std::string& _string) { return _string; }
  };
} // namespace cres

template<typename _TString_>
struct std::formatter<cres::error<_TString_>> : public std::formatter<_TString_>
{
  template<typename FormatContext>
  FormatContext::iterator format(cres::error<_TString_> const& p, FormatContext& ctx) const
  {
    return std::formatter<_TString_>::format(p.get_message(), ctx);
  }
};
