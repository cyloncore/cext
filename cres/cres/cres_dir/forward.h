#include <string>

namespace cres
{
  template<typename _TString_>
  class error;
  class no_error;
  template<typename _T_, typename _E_>
  class result;

  template<typename _E_>
  struct error_traits;

  template<typename _TString_>
  struct string_traits;
} // namespace cres

using cres_error = cres::error<std::string>;

template<typename _T_, typename _E_ = cres_error>
using cres_result = cres::result<_T_, _E_>;

using cresError = cres::error<std::string>;

template<typename _T_, typename _E_ = cresError>
using cresResult = cres::result<_T_, _E_>;
