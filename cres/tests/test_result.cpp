#include <cres>

#include "catch.hpp"

static_assert(not std::is_convertible_v<std::string, cres_result<std::string>>);
static_assert(not std::is_convertible_v<std::string, cres_result<void>>);

namespace
{
  struct A
  {
  };

  struct B
  {
    B(A&&) {}
  };

  cres_result<A> a() { return cres_success(A{}); }

  cres_result<B> b() { return a(); }
  cres_result<B> b2() { return cres_success(A{}); }

} // namespace

namespace
{
  cres_result<int> test_s() { return cres_success(10); }

  cres_result<int> test_st()
  {
    cres_try(int a, test_s());
    return cres_success(a + 1);
  }

  cres_result<void> test_s_2()
  {
    cres_try(cres_ignore, test_s());
    return cres_success();
  }

  cres_result<void> test_s_3()
  {
    cres_try(cres_ignore, test_s_2());
    return cres_success();
  }

  cres_result<int> test_f() { return cres_failure("Failure."); }

  cres_result<void> test_f_2()
  {
    cres_try(cres_ignore, test_f());
    return cres_success();
  }

  cres_result<void> test_f_3(const std::string& _arg) { return cres_failure("Hello {}!", _arg); }

  cres_result<void> test_cres_cond(bool _s) { return cres_cond<void>(_s, "Hello {}!", 10); }
  cres_result<int> test_cres_cond_int(bool _s) { return cres_cond<int>(_s, 12, "Hello {}!", 10); }
  cres_result<void> test_cres_try_void_message()
  {
    cres_try(cres_ignore, test_f(), message("Hello {}: {}.", 10));
    return cres_failure("Failure.");
  }
} // namespace

TEST_CASE("result", "[result]")
{
  CHECK(b().is_successful());
  CHECK(b2().is_successful());

  cres_result<int> res = test_s();
  CHECK(res.is_successful());
  CHECK(res.get_value() == 10);
  CHECK(res.get_error().get_message() == "No error.");

  CHECK(test_s().expect_success() == 10);

  res = test_st();
  CHECK(res.is_successful());
  CHECK(res.get_value() == 11);
  CHECK(res.get_error().get_message() == "No error.");

  res = test_f();
  CHECK_FALSE(res.is_successful());
  CHECK(res.get_error().get_message() == "Failure.");

  cres_result<void> res_2 = test_s_2();
  CHECK(res_2.is_successful());
  CHECK(res_2.get_error().get_message() == "No error.");
  res_2 = test_f_2();
  CHECK_FALSE(res_2.is_successful());
  CHECK(res_2.get_error().get_message() == "Failure.");

  cres_result<void> res_s_3 = test_s_3();
  CHECK(res_s_3.is_successful());
  CHECK(res_s_3.get_error().get_message() == "No error.");

  cres_result<void> res_3 = test_f_3("world");
  CHECK_FALSE(res_3.is_successful());
  CHECK(res_3.get_error().get_message() == "Hello world!");

  // test cres_cond
  res_2 = test_cres_cond(false);
  CHECK_FALSE(res_2.is_successful());
  CHECK(res_2.get_error().get_message() == "Hello 10!");

  res_2 = test_cres_cond(true);
  CHECK(res_2.is_successful());
  CHECK(res_2.get_error().get_message() == "No error.");

  res = test_cres_cond_int(false);
  CHECK_FALSE(res.is_successful());
  CHECK(res.get_error().get_message() == "Hello 10!");

  res = test_cres_cond_int(true);
  CHECK(res.is_successful());
  CHECK(res.get_value() == 12);
  CHECK(res.get_error().get_message() == "No error.");

  // test_cres_try_void_message
  res_2 = test_cres_try_void_message();
  CHECK_FALSE(res_2.is_successful());
  CHECK(res_2.get_error().get_message() == "Hello 10: Failure..");

  res_2 = res.discard_value();
  CHECK(res_2.is_successful());
  CHECK(res_2.get_error().get_message() == "No error.");
}

TEST_CASE("tuple bindings", "[result]")
{
  {
    auto const& [s, r, e] = test_s();
    CHECK(s);
    CHECK(r == 10);
    CHECK(e.value().get_message() == "No error.");
  }
  {
    auto const& [s, r, e] = test_f();
    CHECK_FALSE(s);
    CHECK(e.value().get_message() == "Failure.");
  }
  {
    auto const& [s, e] = test_s_2();
    CHECK(s);
    CHECK(e.value().get_message() == "No error.");
  }
}

TEST_CASE("formatting", "[result]")
{
  CHECK(std::format("{}", cres_error("hello")) == "hello");
  CHECK(std::format("{}", cresError("hello")) == "hello");
}

enum class custom_error_enum
{
  no_value,
  some_value
};

struct custom_error
{
  custom_error() {}
  custom_error(const std::string& _message, custom_error_enum _enum)
      : m_message(_message), m_enum(_enum)
  {
  }
  bool operator!=(const custom_error& _rhs)
  {
    return m_message != _rhs.m_message or m_enum != _rhs.m_enum;
  }
  std::string m_message;
  custom_error_enum m_enum;
};

template<>
struct cres::error_traits<custom_error> : public std::true_type
{
  static const custom_error& invalid()
  {
    static custom_error ce("Error is invalid and was not set properly.",
                           custom_error_enum::no_value);
    return ce;
  }
  static const custom_error& no_error()
  {
    static custom_error no_error("No error.", custom_error_enum::no_value);
    return no_error;
  }
};

template<typename _T_>
using custom_result = cres_result<int, custom_error>;

template<typename... _Args_>
inline custom_error cres_custom_failure(custom_error_enum _enum,
                                        std::format_string<_Args_...> _format, _Args_&&... _args)
{
  return custom_error(std::vformat(_format.get(), std::make_format_args(_args...)), _enum);
}

custom_result<int> test_custom_result(bool _fail)
{
  if(_fail)
  {
    return cres_custom_failure(custom_error_enum::some_value, "{}", _fail);
  }
  else
  {
    return cres_success(2);
  }
}

TEST_CASE("custom_error", "[result]")
{
  custom_result<int> cr;

  cr = test_custom_result(true);
  CHECK(not cr.is_successful());
  CHECK(cr.get_error().m_enum == custom_error_enum::some_value);
  CHECK(cr.get_error().m_message == "true");

  cr = test_custom_result(false);
  CHECK(cr.is_successful());
  CHECK(cr.get_value() == 2);
}
