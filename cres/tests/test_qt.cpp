#include <cres_qt>

#include <QJsonObject>
#include <QJsonValue>

#include "catch.hpp"

static_assert(std::is_convertible_v<cres_qerror, cresError>);
static_assert(std::is_convertible_v<cres_qerror, cres_qresult<int>>);
static_assert(std::is_convertible_v<cresError, cres_qresult<int>>);
static_assert(std::is_convertible_v<cres::result<QJsonObject, cres::no_error>,
                                    cres::result<QJsonValue, cres_qerror>>);
static_assert(std::is_convertible_v<cresResult<int>, cres_qresult<int>>);
static_assert(not std::is_convertible_v<QString, cres_qresult<QString>>);
static_assert(not std::is_convertible_v<QString, cres_qresult<void>>);

namespace
{
  cres_qresult<int> test_s() { return cres_success(10); }

  cres_qresult<void> test_s_2()
  {
    cres_try(cres_ignore, test_s());
    return cres_success();
  }

  cres_qresult<int> test_f() { return cres_failure("Failure."); }

  cres_qresult<void> test_f_2()
  {
    cres_try(cres_ignore, test_f());
    return cres_success();
  }

  cres_qresult<void> test_f_3(const std::string& _arg) { return cres_failure("Hello {}!", _arg); }
} // namespace

TEST_CASE("result qt", "[result]")
{
  cres_qresult<int> res = test_s();
  CHECK(res.is_successful());
  CHECK(res.get_value() == 10);
  CHECK(res.get_error().get_message() == "No error.");
  res = test_f();
  CHECK_FALSE(res.is_successful());
  CHECK(res.get_error().get_message() == "Failure.");

  cres_qresult<void> res_2 = test_s_2();
  CHECK(res_2.is_successful());
  CHECK(res_2.get_error().get_message() == "No error.");
  res_2 = test_f_2();
  CHECK_FALSE(res_2.is_successful());
  CHECK(res_2.get_error().get_message() == "Failure.");

  cres_qresult<void> res_3 = test_f_3("world");
  CHECK_FALSE(res_3.is_successful());
  CHECK(res_3.get_error().get_message() == "Hello world!");

  res_3 = res.discard_value();
  CHECK_FALSE(res_3.is_successful());
  CHECK(res_3.get_error().get_message() == "Failure.");
}

namespace cres_qt_test
{
  // For testing compilation
  cres_qresult<QJsonValue> toJsonValue2() { return cres_failure("Should not be called by tests"); }
  cres_qresult<QJsonValue> toJsonValue()
  {
    cres_try(QJsonValue value, toJsonValue2());
    return cres_success(value);
  }

  cres_qresult<void> test_cres_cond_void()
  {
    return cres_cond<void>(false, "Failed to {}", "be polite");
  }
} // namespace cres_qt_test
