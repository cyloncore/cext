#define __cext_pragma__(x) _Pragma(#x)

#ifdef __clang__
#define cext_clang_pragma(str) __cext_pragma__(clang str)
#else
#define cext_clang_pragma(str)

#endif