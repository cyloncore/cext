#include <type_traits>

namespace cext_impl
{
  struct named_type_options
  {
    enum
    {
      none = 0,
      /**
       * indicates that a \ref named_type<_T_> cannot be cast explicitly to a \ref _T_.
       * The following code will fail:
       * @code
       * using MyInt  = cext_named_type<int, struct MyIntTag,
       * Named_type_options::explicit_conversion>; MyInt myInt(1); int a = myInt; // this will
       * report an error int b = myInt.get_value(); // this will compile
       * @endcode
       */
      explicit_conversion = 1,
      default_options = none,
    };
  };
  /**
   * Allow to define a \ref named_type. This is usefull for function that
   * takes argument with the same type but for which it is important to
   * distinguish between values (for instance latitude vs longitude).
   *
   * Example of use:
   * @code
   * using Latitude  = cext_named_type<qreal, struct LatitudeTag>;
   * using Longitude = cext_named_type<qreal, struct LongitudeTag>;
   * using Altitude  = cext_named_type<qreal, struct AltitudeTag>;
   * Point createPoint(Latitude _latiude, Longitude _longitude, Altitude _altitude);
   * @endcode
   */
  template<typename _T_, typename _Tag_, int _Options_ = named_type_options::default_options>
  class named_type
  {
  public:
    explicit named_type(const _T_& _value) : m_value(_value) {}
    template<int _Options_2_ = _Options_>
      requires((_Options_2_ & named_type_options::explicit_conversion) == 0)
    operator _T_() const
    {
      return m_value;
    }
    const _T_* operator->() const { return &m_value; }
    _T_ get_value() const { return m_value; }
  private:
    _T_ m_value;
  };

#define CEXT_MAKE_NT_OPERATOR(_OP_)                                                                \
  template<typename _T_, typename _Tag_, int _Options_>                                            \
  auto operator _OP_(const named_type<_T_, _Tag_, _Options_>& _lhs, _T_ _rhs)                      \
  {                                                                                                \
    return _T_(_lhs) _OP_ _rhs;                                                                    \
  }

  CEXT_MAKE_NT_OPERATOR(>=)
  CEXT_MAKE_NT_OPERATOR(<=)
  CEXT_MAKE_NT_OPERATOR(>)
  CEXT_MAKE_NT_OPERATOR(<)
  CEXT_MAKE_NT_OPERATOR(==)
  CEXT_MAKE_NT_OPERATOR(!=)
  CEXT_MAKE_NT_OPERATOR(+)
  CEXT_MAKE_NT_OPERATOR(-)
  CEXT_MAKE_NT_OPERATOR(*)
  CEXT_MAKE_NT_OPERATOR(/)

#undef CEXT_MAKE_NT_OPERATOR

} // namespace cext_impl

#include <format>

template<typename _T_, typename _Tag_, int _Options_>
struct std::formatter<cext_impl::named_type<_T_, _Tag_, _Options_>> : public std::formatter<_T_>
{
  using super = std::formatter<_T_>;
  template<typename FormatContext>
  FormatContext::iterator format(cext_impl::named_type<_T_, _Tag_, _Options_> const& p,
                                 FormatContext& ctx) const
  {
    return super::format(p.get_value(), ctx);
  }
};

namespace std
{
  // named_types are trivial if their base type is trivial
  template<typename _T_, typename _Tag_, int _Options_>
  struct is_trivial<cext_impl::named_type<_T_, _Tag_, _Options_>> : public std::is_trivial<_T_>
  {
  };
} // namespace std
